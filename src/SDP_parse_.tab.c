/* A Bison parser, made by GNU Bison 2.5.  */

/* Bison implementation for Yacc-like parsers in C
   
      Copyright (C) 1984, 1989-1990, 2000-2011 Free Software Foundation, Inc.
   
   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.
   
   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.
   
   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>.  */

/* As a special exception, you may create a larger work that contains
   part or all of the Bison parser skeleton and distribute that work
   under terms of your choice, so long as that work isn't itself a
   parser generator using the skeleton or a modified version thereof
   as a parser skeleton.  Alternatively, if you modify or redistribute
   the parser skeleton itself, you may (at your option) remove this
   special exception, which will cause the skeleton and the resulting
   Bison output files to be licensed under the GNU General Public
   License without this special exception.
   
   This special exception was added by the Free Software Foundation in
   version 2.2 of Bison.  */

/* C LALR(1) parser skeleton written by Richard Stallman, by
   simplifying the original so-called "semantic" parser.  */

/* All symbols defined below should begin with yy or YY, to avoid
   infringing on user name space.  This should be done even for local
   variables, as they might otherwise be expanded by user macros.
   There are some unavoidable exceptions within include files to
   define necessary library symbols; they are noted "INFRINGES ON
   USER NAME SPACE" below.  */

/* Identify Bison output.  */
#define YYBISON 1

/* Bison version.  */
#define YYBISON_VERSION "2.5"

/* Skeleton name.  */
#define YYSKELETON_NAME "yacc.c"

/* Pure parsers.  */
#define YYPURE 0

/* Push parsers.  */
#define YYPUSH 0

/* Pull parsers.  */
#define YYPULL 1

/* Using locations.  */
#define YYLSP_NEEDED 0

/* Substitute the variable and function names.  */
#define yyparse         SDP_parse_parse
#define yylex           SDP_parse_lex
#define yyerror         SDP_parse_error
#define yylval          SDP_parse_lval
#define yychar          SDP_parse_char
#define yydebug         SDP_parse_debug
#define yynerrs         SDP_parse_nerrs


/* Copy the first part of user declarations.  */

/* Line 268 of yacc.c  */
#line 1 "SDP_parser.y"

///////////////////////////////////////////////////////////////////////////////
//
// Copyright (c) 2000-2023 Ericsson Telecom AB
//
// All rights reserved. This program and the accompanying materials
// are made available under the terms of the Eclipse Public License v2.0
// which accompanies this distribution, and is available at
// https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
///////////////////////////////////////////////////////////////////////////////

//
//  File:               SDP_parser.y
//  Rev:                R14C
//  Prodnr:             CNL 113 353
//  Contact:            http://ttcn.ericsson.se
//  Reference:          ITU-T SDP
/*C declarations*/

#include "SDP_parse_parser.h"
#include <stdarg.h>
#define YYDEBUG 1
using namespace SDP__Types;

extern int SDP_parse_lex();
extern int SDP_parse_error(char *s);

SDP__Message* SDP_parse_parsed_message;

SDP__Message* SDP_parse_get_parsed_message() {
	return SDP_parse_parsed_message;
}

void SDP_parse_set_parsed_message(SDP__Message* pdu) {
	SDP_parse_parsed_message=pdu;
}




/* Line 268 of yacc.c  */
#line 120 "SDP_parse_.tab.c"

/* Enabling traces.  */
#ifndef YYDEBUG
# define YYDEBUG 0
#endif

/* Enabling verbose error messages.  */
#ifdef YYERROR_VERBOSE
# undef YYERROR_VERBOSE
# define YYERROR_VERBOSE 1
#else
# define YYERROR_VERBOSE 0
#endif

/* Enabling the token table.  */
#ifndef YYTOKEN_TABLE
# define YYTOKEN_TABLE 0
#endif


/* Tokens.  */
#ifndef YYTOKENTYPE
# define YYTOKENTYPE
   /* Put the tokens into the symbol table, so that GDB and other debuggers
      know about them.  */
   enum yytokentype {
     _ZERO = 258,
     _ONE = 259,
     _TWO = 260,
     _THREE = 261,
     _FOUR = 262,
     _FIVE = 263,
     _SIX = 264,
     _SEVEN = 265,
     _EIGHT = 266,
     _NINE = 267,
     CRLF = 268,
     ALPHA = 269,
     SPACE = 270,
     UNDERSCORE = 271,
     PERIOD = 272,
     ANYBYTE = 273,
     LBRACE = 274,
     RBRACE = 275,
     LT = 276,
     GT = 277,
     PLUS = 278,
     HYPHEN = 279,
     SLASH = 280,
     COLON = 281,
     EMAIL = 282,
     TOKEN = 283,
     INTTOKEN = 284,
     VEQ = 285,
     OEQ = 286,
     SEQ = 287,
     IEQ = 288,
     UEQ = 289,
     EEQ = 290,
     PEQ = 291,
     CEQ = 292,
     BEQ = 293,
     BW_CT = 294,
     BW_AS = 295,
     TEQ = 296,
     ZEQ = 297,
     REQ = 298,
     KEQ = 299,
     AEQ = 300,
     MEQ = 301,
     A_CAT = 302,
     A_KEYWDS = 303,
     A_TOOL = 304,
     A_PTIME = 305,
     A_RECVONLY = 306,
     A_SENDRECV = 307,
     A_SENDONLY = 308,
     A_ORIENT = 309,
     A_TYPE = 310,
     A_CHARSET = 311,
     A_SDLANG = 312,
     A_LANG = 313,
     A_FRAMERATE = 314,
     A_QUALITY = 315,
     A_FMTP = 316,
     A_CURR = 317,
     A_DES = 318,
     A_CONF = 319,
     A_RTMAP = 320,
     A_RTCP = 321,
     A_MAX_SIZE = 322,
     A_PATH = 323,
     A_ACCEPT_TYPES = 324,
     A_ACCEPT_WRAPPED_TYPES = 325,
     A_MAXPRATE = 326,
     A_MID = 327,
     A_GROUP = 328,
     A_FILE_SELECTOR = 329,
     A_FILE_TRANSFER_ID = 330,
     A_INACTIVE = 331,
     A_SETUP = 332,
     A_CONNECTION = 333,
     A_CRYPTO = 334,
     A_CONTENT = 335,
     A_LABEL = 336,
     A_FLOORCTRL = 337,
     A_CONFID = 338,
     A_USERID = 339,
     A_FLOORID = 340,
     A_FINGERPRINT = 341,
     A_ICE_UFRAG = 342,
     A_ICE_PWD = 343,
     A_CANDIDATE = 344,
     A_ICE_LITE = 345,
     A_ICE_MISMATCH = 346,
     A_REMOTE_CANDIDATE = 347,
     A_ICE_OPTIONS = 348,
     A_RTCP_FB = 349,
     A_MAXPTIME = 350,
     A_T38_VERSION = 351,
     A_T38_BITRATE = 352,
     A_T38_BITREMOVAL = 353,
     A_T38_MMR = 354,
     A_T38_JBIG = 355,
     A_T38_RATEMANAGEMENT = 356,
     A_T38_MAXBUFFER = 357,
     A_T38_MAXDATAGRAM = 358,
     A_T38_IFP = 359,
     A_T38_EC = 360,
     A_T38_ECDEPTH = 361,
     A_T38_FEC = 362,
     A_T38_VENDORINFO = 363,
     A_T38_MODEM = 364,
     A_FILE_DISPOSITION = 365,
     A_FILE_DATE = 366,
     A_FILE_ICON = 367,
     A_FILE_RANGE = 368,
     A_SCTPMAP = 369,
     A_SCTPPORT = 370,
     A_MAX_MSG_SIZE = 371,
     A_RTCP_MUX = 372
   };
#endif



#if ! defined YYSTYPE && ! defined YYSTYPE_IS_DECLARED
typedef union YYSTYPE
{

/* Line 293 of yacc.c  */
#line 43 "SDP_parser.y"

	int number;
	INTEGER* intnum;
	char byte;
	CHARSTRING* t_charstring;
	SDP__Types::SDP__Message* t_sdp_message;
	SDP__Types::SDP__Origin* t_sdp_origin;
	SDP__Types::SDP__contact* t_sdp_contact;
	SDP__Types::SDP__email__list* t_sdp_email_list;
	SDP__Types::SDP__phone__list* t_sdp_phone_list;
	SDP__Types::SDP__connection* t_sdp_connection;
	SDP__Types::SDP__connection__list* t_sdp_connection_list;
	SDP__Types::SDP__conn__addr* t_sdp_conn_addr;
	SDP__Types::SDP__bandwidth* t_sdp_bandwidth;
	SDP__Types::SDP__time__list* t_sdp_time_list;
	SDP__Types::SDP__time* t_sdp_time;
	SDP__Types::SDP__time__field* t_sdp_time_field;
	SDP__Types::SDP__repeat* t_sdp_repeat;
	SDP__Types::SDP__repeat__list* t_sdp_repeat_list;
	SDP__Types::SDP__timezone__list* t_sdp_timezone_list;
	SDP__Types::SDP__timezone* t_sdp_timezone;
	SDP__Types::SDP__typed__time* t_sdp_typed_time;
	SDP__Types::SDP__typed__time__list* t_sdp_typed_time_list;
	SDP__Types::SDP__key* t_sdp_key;
	SDP__Types::SDP__attribute* t_sdp_attribute;
	SDP__Types::SDP__attribute__list* t_sdp_attribute_list;
	SDP__Types::SDP__media__desc* t_sdp_media_desc;
	SDP__Types::SDP__media__desc__list* t_sdp_media_desc_list;
	SDP__Types::SDP__media__field* t_sdp_media_field;
	SDP__Types::SDP__fmt__list* t_sdp_fmt_list;
	SDP__Types::SDP__bandwidth__list* t_bandwidth_list;
	SDP__Types::SDP__url__list* t_url_list;
	SDP__Types::SDP__media__type__list* t_type_list;
	SDP__Types::SDP__id__tag__list* t_id_list;
	SDP__Types::SDP__Remote__candidate__list* t_candidate_list;	
	SDP__Types::SDP__Remote__candidate* t_candidate;	
	SDP__Types::SDP__extension__list* t_extension_list;	
	SDP__Types::SDP__extension* t_extension;
	SDP__Types::SDP__ice__options__list* t_sdp_ice_option_list;		



/* Line 293 of yacc.c  */
#line 316 "SDP_parse_.tab.c"
} YYSTYPE;
# define YYSTYPE_IS_TRIVIAL 1
# define yystype YYSTYPE /* obsolescent; will be withdrawn */
# define YYSTYPE_IS_DECLARED 1
#endif


/* Copy the second part of user declarations.  */


/* Line 343 of yacc.c  */
#line 328 "SDP_parse_.tab.c"

#ifdef short
# undef short
#endif

#ifdef YYTYPE_UINT8
typedef YYTYPE_UINT8 yytype_uint8;
#else
typedef unsigned char yytype_uint8;
#endif

#ifdef YYTYPE_INT8
typedef YYTYPE_INT8 yytype_int8;
#elif (defined __STDC__ || defined __C99__FUNC__ \
     || defined __cplusplus || defined _MSC_VER)
typedef signed char yytype_int8;
#else
typedef short int yytype_int8;
#endif

#ifdef YYTYPE_UINT16
typedef YYTYPE_UINT16 yytype_uint16;
#else
typedef unsigned short int yytype_uint16;
#endif

#ifdef YYTYPE_INT16
typedef YYTYPE_INT16 yytype_int16;
#else
typedef short int yytype_int16;
#endif

#ifndef YYSIZE_T
# ifdef __SIZE_TYPE__
#  define YYSIZE_T __SIZE_TYPE__
# elif defined size_t
#  define YYSIZE_T size_t
# elif ! defined YYSIZE_T && (defined __STDC__ || defined __C99__FUNC__ \
     || defined __cplusplus || defined _MSC_VER)
#  include <stddef.h> /* INFRINGES ON USER NAME SPACE */
#  define YYSIZE_T size_t
# else
#  define YYSIZE_T unsigned int
# endif
#endif

#define YYSIZE_MAXIMUM ((YYSIZE_T) -1)

#ifndef YY_
# if defined YYENABLE_NLS && YYENABLE_NLS
#  if ENABLE_NLS
#   include <libintl.h> /* INFRINGES ON USER NAME SPACE */
#   define YY_(msgid) dgettext ("bison-runtime", msgid)
#  endif
# endif
# ifndef YY_
#  define YY_(msgid) msgid
# endif
#endif

/* Suppress unused-variable warnings by "using" E.  */
#if ! defined lint || defined __GNUC__
# define YYUSE(e) ((void) (e))
#else
# define YYUSE(e) /* empty */
#endif

/* Identity function, used to suppress warnings about constant conditions.  */
#ifndef lint
# define YYID(n) (n)
#else
#if (defined __STDC__ || defined __C99__FUNC__ \
     || defined __cplusplus || defined _MSC_VER)
static int
YYID (int yyi)
#else
static int
YYID (yyi)
    int yyi;
#endif
{
  return yyi;
}
#endif

#if ! defined yyoverflow || YYERROR_VERBOSE

/* The parser invokes alloca or malloc; define the necessary symbols.  */

# ifdef YYSTACK_USE_ALLOCA
#  if YYSTACK_USE_ALLOCA
#   ifdef __GNUC__
#    define YYSTACK_ALLOC __builtin_alloca
#   elif defined __BUILTIN_VA_ARG_INCR
#    include <alloca.h> /* INFRINGES ON USER NAME SPACE */
#   elif defined _AIX
#    define YYSTACK_ALLOC __alloca
#   elif defined _MSC_VER
#    include <malloc.h> /* INFRINGES ON USER NAME SPACE */
#    define alloca _alloca
#   else
#    define YYSTACK_ALLOC alloca
#    if ! defined _ALLOCA_H && ! defined EXIT_SUCCESS && (defined __STDC__ || defined __C99__FUNC__ \
     || defined __cplusplus || defined _MSC_VER)
#     include <stdlib.h> /* INFRINGES ON USER NAME SPACE */
#     ifndef EXIT_SUCCESS
#      define EXIT_SUCCESS 0
#     endif
#    endif
#   endif
#  endif
# endif

# ifdef YYSTACK_ALLOC
   /* Pacify GCC's `empty if-body' warning.  */
#  define YYSTACK_FREE(Ptr) do { /* empty */; } while (YYID (0))
#  ifndef YYSTACK_ALLOC_MAXIMUM
    /* The OS might guarantee only one guard page at the bottom of the stack,
       and a page size can be as small as 4096 bytes.  So we cannot safely
       invoke alloca (N) if N exceeds 4096.  Use a slightly smaller number
       to allow for a few compiler-allocated temporary stack slots.  */
#   define YYSTACK_ALLOC_MAXIMUM 4032 /* reasonable circa 2006 */
#  endif
# else
#  define YYSTACK_ALLOC YYMALLOC
#  define YYSTACK_FREE YYFREE
#  ifndef YYSTACK_ALLOC_MAXIMUM
#   define YYSTACK_ALLOC_MAXIMUM YYSIZE_MAXIMUM
#  endif
#  if (defined __cplusplus && ! defined EXIT_SUCCESS \
       && ! ((defined YYMALLOC || defined malloc) \
	     && (defined YYFREE || defined free)))
#   include <stdlib.h> /* INFRINGES ON USER NAME SPACE */
#   ifndef EXIT_SUCCESS
#    define EXIT_SUCCESS 0
#   endif
#  endif
#  ifndef YYMALLOC
#   define YYMALLOC malloc
#   if ! defined malloc && ! defined EXIT_SUCCESS && (defined __STDC__ || defined __C99__FUNC__ \
     || defined __cplusplus || defined _MSC_VER)
void *malloc (YYSIZE_T); /* INFRINGES ON USER NAME SPACE */
#   endif
#  endif
#  ifndef YYFREE
#   define YYFREE free
#   if ! defined free && ! defined EXIT_SUCCESS && (defined __STDC__ || defined __C99__FUNC__ \
     || defined __cplusplus || defined _MSC_VER)
void free (void *); /* INFRINGES ON USER NAME SPACE */
#   endif
#  endif
# endif
#endif /* ! defined yyoverflow || YYERROR_VERBOSE */


#if (! defined yyoverflow \
     && (! defined __cplusplus \
	 || (defined YYSTYPE_IS_TRIVIAL && YYSTYPE_IS_TRIVIAL)))

/* A type that is properly aligned for any stack member.  */
union yyalloc
{
  yytype_int16 yyss_alloc;
  YYSTYPE yyvs_alloc;
};

/* The size of the maximum gap between one aligned stack and the next.  */
# define YYSTACK_GAP_MAXIMUM (sizeof (union yyalloc) - 1)

/* The size of an array large to enough to hold all stacks, each with
   N elements.  */
# define YYSTACK_BYTES(N) \
     ((N) * (sizeof (yytype_int16) + sizeof (YYSTYPE)) \
      + YYSTACK_GAP_MAXIMUM)

# define YYCOPY_NEEDED 1

/* Relocate STACK from its old location to the new one.  The
   local variables YYSIZE and YYSTACKSIZE give the old and new number of
   elements in the stack, and YYPTR gives the new location of the
   stack.  Advance YYPTR to a properly aligned location for the next
   stack.  */
# define YYSTACK_RELOCATE(Stack_alloc, Stack)				\
    do									\
      {									\
	YYSIZE_T yynewbytes;						\
	YYCOPY (&yyptr->Stack_alloc, Stack, yysize);			\
	Stack = &yyptr->Stack_alloc;					\
	yynewbytes = yystacksize * sizeof (*Stack) + YYSTACK_GAP_MAXIMUM; \
	yyptr += yynewbytes / sizeof (*yyptr);				\
      }									\
    while (YYID (0))

#endif

#if defined YYCOPY_NEEDED && YYCOPY_NEEDED
/* Copy COUNT objects from FROM to TO.  The source and destination do
   not overlap.  */
# ifndef YYCOPY
#  if defined __GNUC__ && 1 < __GNUC__
#   define YYCOPY(To, From, Count) \
      __builtin_memcpy (To, From, (Count) * sizeof (*(From)))
#  else
#   define YYCOPY(To, From, Count)		\
      do					\
	{					\
	  YYSIZE_T yyi;				\
	  for (yyi = 0; yyi < (Count); yyi++)	\
	    (To)[yyi] = (From)[yyi];		\
	}					\
      while (YYID (0))
#  endif
# endif
#endif /* !YYCOPY_NEEDED */

/* YYFINAL -- State number of the termination state.  */
#define YYFINAL  5
/* YYLAST -- Last index in YYTABLE.  */
#define YYLAST   453

/* YYNTOKENS -- Number of terminals.  */
#define YYNTOKENS  118
/* YYNNTS -- Number of nonterminals.  */
#define YYNNTS  55
/* YYNRULES -- Number of rules.  */
#define YYNRULES  184
/* YYNRULES -- Number of states.  */
#define YYNSTATES  389

/* YYTRANSLATE(YYLEX) -- Bison symbol number corresponding to YYLEX.  */
#define YYUNDEFTOK  2
#define YYMAXUTOK   372

#define YYTRANSLATE(YYX)						\
  ((unsigned int) (YYX) <= YYMAXUTOK ? yytranslate[YYX] : YYUNDEFTOK)

/* YYTRANSLATE[YYLEX] -- Bison symbol number corresponding to YYLEX.  */
static const yytype_uint8 yytranslate[] =
{
       0,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     1,     2,     3,     4,
       5,     6,     7,     8,     9,    10,    11,    12,    13,    14,
      15,    16,    17,    18,    19,    20,    21,    22,    23,    24,
      25,    26,    27,    28,    29,    30,    31,    32,    33,    34,
      35,    36,    37,    38,    39,    40,    41,    42,    43,    44,
      45,    46,    47,    48,    49,    50,    51,    52,    53,    54,
      55,    56,    57,    58,    59,    60,    61,    62,    63,    64,
      65,    66,    67,    68,    69,    70,    71,    72,    73,    74,
      75,    76,    77,    78,    79,    80,    81,    82,    83,    84,
      85,    86,    87,    88,    89,    90,    91,    92,    93,    94,
      95,    96,    97,    98,    99,   100,   101,   102,   103,   104,
     105,   106,   107,   108,   109,   110,   111,   112,   113,   114,
     115,   116,   117
};

#if YYDEBUG
/* YYPRHS[YYN] -- Index of the first RHS symbol of rule number YYN in
   YYRHS.  */
static const yytype_uint16 yyprhs[] =
{
       0,     0,     3,    18,    19,    22,    29,    41,    51,    53,
      57,    59,    63,    64,    67,    71,    74,    77,    80,    83,
      85,    87,    89,    92,    95,    98,   101,   104,   107,   110,
     113,   116,   119,   122,   125,   128,   131,   134,   137,   140,
     143,   148,   151,   154,   159,   162,   165,   167,   169,   172,
     175,   178,   181,   184,   187,   190,   193,   196,   199,   202,
     205,   225,   227,   229,   232,   235,   238,   241,   244,   247,
     250,   253,   256,   259,   262,   265,   268,   271,   274,   279,
     282,   289,   292,   295,   298,   301,   304,   307,   310,   313,
     317,   319,   321,   325,   327,   331,   337,   338,   343,   344,
     349,   350,   353,   358,   360,   364,   366,   370,   372,   376,
     377,   381,   383,   387,   389,   392,   395,   396,   399,   407,
     409,   413,   414,   418,   420,   424,   428,   430,   433,   439,
     440,   443,   449,   450,   453,   454,   462,   470,   472,   476,
     482,   483,   486,   490,   492,   497,   502,   504,   505,   508,
     512,   514,   520,   526,   530,   531,   535,   536,   540,   544,
     548,   562,   564,   567,   569,   571,   573,   575,   577,   579,
     581,   583,   585,   587,   589,   591,   593,   595,   597,   599,
     601,   603,   605,   607,   609
};

/* YYRHS -- A `-1'-separated list of the rules' RHS.  */
static const yytype_int16 yyrhs[] =
{
     119,     0,    -1,   166,   167,   165,   164,   163,   160,   156,
     153,   150,   140,   145,   138,   125,   120,    -1,    -1,   120,
     121,    -1,   122,   164,   152,   150,   138,   125,    -1,    46,
      28,    15,    28,    25,    28,    15,   123,    15,   124,    13,
      -1,    46,    28,    15,    28,    15,   123,    15,   124,    13,
      -1,    28,    -1,   123,    25,    28,    -1,    28,    -1,   124,
      15,    28,    -1,    -1,   125,   126,    -1,    45,   127,    13,
      -1,    47,    28,    -1,    48,    28,    -1,    49,    28,    -1,
      50,    28,    -1,    51,    -1,    52,    -1,    53,    -1,    54,
      28,    -1,    55,    28,    -1,    56,    28,    -1,    57,    28,
      -1,    58,    28,    -1,    59,    28,    -1,    60,    28,    -1,
      61,    28,    -1,    62,    28,    -1,    63,    28,    -1,    64,
      28,    -1,    65,    28,    -1,    66,    28,    -1,    67,    28,
      -1,    68,   135,    -1,    69,   137,    -1,    70,   137,    -1,
      71,    28,    -1,    71,    28,    17,    28,    -1,    72,    28,
      -1,    73,    28,    -1,    73,    28,    15,   136,    -1,    74,
      28,    -1,    75,    28,    -1,    76,    -1,   117,    -1,    77,
      28,    -1,    78,    28,    -1,    79,    28,    -1,    80,    28,
      -1,    81,    28,    -1,    82,    28,    -1,    83,    28,    -1,
      84,    28,    -1,    85,    28,    -1,    86,    28,    -1,    87,
      28,    -1,    88,    28,    -1,    89,    28,    15,    28,    15,
      28,    15,    28,    15,    28,    15,    28,    15,    28,    15,
      28,   131,   132,   133,    -1,    90,    -1,    91,    -1,    92,
     129,    -1,    93,   128,    -1,    94,    28,    -1,    95,    28,
      -1,    96,    28,    -1,    97,    28,    -1,    98,    28,    -1,
      99,    28,    -1,   100,    28,    -1,   101,    28,    -1,   102,
      28,    -1,   103,    28,    -1,   104,    28,    -1,   105,    28,
      -1,   106,    28,    -1,   106,    28,    15,    28,    -1,   107,
      28,    -1,   108,    28,    15,    28,    15,    28,    -1,   109,
      28,    -1,   110,    28,    -1,   111,    28,    -1,   112,    28,
      -1,   113,    28,    -1,   114,    28,    -1,   115,    28,    -1,
     116,    28,    -1,    28,    26,    28,    -1,    28,    -1,    28,
      -1,   128,    15,    28,    -1,   130,    -1,   129,    15,   130,
      -1,    28,    15,    28,    15,    28,    -1,    -1,    15,    28,
      15,    28,    -1,    -1,    15,    28,    15,    28,    -1,    -1,
     133,   134,    -1,    15,    28,    15,    28,    -1,    28,    -1,
     135,    15,    28,    -1,    28,    -1,   136,    15,    28,    -1,
      28,    -1,   137,    15,    28,    -1,    -1,    44,   139,    13,
      -1,    28,    -1,    28,    26,    28,    -1,   141,    -1,   140,
     141,    -1,   149,   142,    -1,    -1,   142,   143,    -1,    43,
     148,    15,   148,    15,   144,    13,    -1,   148,    -1,   144,
      15,   148,    -1,    -1,    42,   146,    13,    -1,   147,    -1,
     146,    15,   147,    -1,    29,    15,   148,    -1,    29,    -1,
      29,    14,    -1,    41,    28,    15,    28,    13,    -1,    -1,
     150,   151,    -1,    38,    28,    26,    28,    13,    -1,    -1,
     152,   154,    -1,    -1,    37,    28,    15,    28,    15,   155,
      13,    -1,    37,    28,    15,    28,    15,   155,    13,    -1,
      28,    -1,    28,    25,    28,    -1,    28,    25,    28,    25,
      28,    -1,    -1,   156,   157,    -1,    36,   158,    13,    -1,
     159,    -1,   159,    19,   168,    20,    -1,   159,    21,   159,
      22,    -1,   168,    -1,    -1,   160,   161,    -1,    35,   162,
      13,    -1,    27,    -1,    27,    15,    19,   168,    20,    -1,
     168,    15,    21,    27,    22,    -1,    21,    27,    22,    -1,
      -1,    34,    28,    13,    -1,    -1,    33,    28,    13,    -1,
      32,    28,    13,    -1,    30,    29,    13,    -1,    31,    28,
      15,    28,    15,    28,    15,    28,    15,    28,    15,    28,
      13,    -1,   172,    -1,   168,   172,    -1,    14,    -1,   170,
      -1,     3,    -1,   171,    -1,     4,    -1,     5,    -1,     6,
      -1,     7,    -1,     8,    -1,     9,    -1,    10,    -1,    11,
      -1,    12,    -1,   169,    -1,    16,    -1,    17,    -1,    24,
      -1,    23,    -1,    25,    -1,    26,    -1,    18,    -1,    15,
      -1
};

/* YYRLINE[YYN] -- source line where rule number YYN was defined.  */
static const yytype_uint16 yyrline[] =
{
       0,   169,   169,   258,   259,   271,   308,   322,   336,   337,
     345,   350,   357,   360,   373,   377,   382,   387,   392,   397,
     401,   405,   409,   414,   419,   424,   429,   434,   439,   444,
     449,   454,   459,   464,   469,   474,   479,   484,   489,   494,
     500,   507,   512,   518,   525,   530,   535,   539,   543,   548,
     553,   558,   563,   568,   573,   578,   583,   588,   593,   598,
     603,   639,   643,   647,   653,   658,   663,   668,   673,   678,
     683,   688,   693,   698,   703,   708,   713,   718,   724,   731,
     736,   745,   750,   755,   760,   765,   770,   775,   780,   786,
     793,   800,   805,   812,   817,   824,   834,   835,   841,   842,
     847,   848,   861,   870,   875,   882,   887,   894,   899,   907,
     908,   913,   919,   929,   934,   941,   952,   953,   967,   978,
     983,   989,   990,   994,   999,  1006,  1014,  1020,  1027,  1035,
    1036,  1050,  1059,  1060,  1074,  1075,  1086,  1095,  1102,  1110,
    1121,  1122,  1134,  1138,  1144,  1152,  1161,  1178,  1179,  1192,
    1196,  1203,  1210,  1217,  1227,  1228,  1233,  1234,  1238,  1242,
    1244,  1266,  1267,  1276,  1277,  1280,  1281,  1283,  1284,  1285,
    1286,  1287,  1288,  1289,  1290,  1291,  1295,  1296,  1297,  1298,
    1299,  1300,  1301,  1302,  1303
};
#endif

#if YYDEBUG || YYERROR_VERBOSE || YYTOKEN_TABLE
/* YYTNAME[SYMBOL-NUM] -- String name of the symbol SYMBOL-NUM.
   First, the terminals, then, starting at YYNTOKENS, nonterminals.  */
static const char *const yytname[] =
{
  "$end", "error", "$undefined", "_ZERO", "_ONE", "_TWO", "_THREE",
  "_FOUR", "_FIVE", "_SIX", "_SEVEN", "_EIGHT", "_NINE", "CRLF", "ALPHA",
  "SPACE", "UNDERSCORE", "PERIOD", "ANYBYTE", "LBRACE", "RBRACE", "LT",
  "GT", "PLUS", "HYPHEN", "SLASH", "COLON", "EMAIL", "TOKEN", "INTTOKEN",
  "VEQ", "OEQ", "SEQ", "IEQ", "UEQ", "EEQ", "PEQ", "CEQ", "BEQ", "BW_CT",
  "BW_AS", "TEQ", "ZEQ", "REQ", "KEQ", "AEQ", "MEQ", "A_CAT", "A_KEYWDS",
  "A_TOOL", "A_PTIME", "A_RECVONLY", "A_SENDRECV", "A_SENDONLY",
  "A_ORIENT", "A_TYPE", "A_CHARSET", "A_SDLANG", "A_LANG", "A_FRAMERATE",
  "A_QUALITY", "A_FMTP", "A_CURR", "A_DES", "A_CONF", "A_RTMAP", "A_RTCP",
  "A_MAX_SIZE", "A_PATH", "A_ACCEPT_TYPES", "A_ACCEPT_WRAPPED_TYPES",
  "A_MAXPRATE", "A_MID", "A_GROUP", "A_FILE_SELECTOR",
  "A_FILE_TRANSFER_ID", "A_INACTIVE", "A_SETUP", "A_CONNECTION",
  "A_CRYPTO", "A_CONTENT", "A_LABEL", "A_FLOORCTRL", "A_CONFID",
  "A_USERID", "A_FLOORID", "A_FINGERPRINT", "A_ICE_UFRAG", "A_ICE_PWD",
  "A_CANDIDATE", "A_ICE_LITE", "A_ICE_MISMATCH", "A_REMOTE_CANDIDATE",
  "A_ICE_OPTIONS", "A_RTCP_FB", "A_MAXPTIME", "A_T38_VERSION",
  "A_T38_BITRATE", "A_T38_BITREMOVAL", "A_T38_MMR", "A_T38_JBIG",
  "A_T38_RATEMANAGEMENT", "A_T38_MAXBUFFER", "A_T38_MAXDATAGRAM",
  "A_T38_IFP", "A_T38_EC", "A_T38_ECDEPTH", "A_T38_FEC",
  "A_T38_VENDORINFO", "A_T38_MODEM", "A_FILE_DISPOSITION", "A_FILE_DATE",
  "A_FILE_ICON", "A_FILE_RANGE", "A_SCTPMAP", "A_SCTPPORT",
  "A_MAX_MSG_SIZE", "A_RTCP_MUX", "$accept", "announcement",
  "media_descriptions", "media_description", "media_field", "transport",
  "fmt_list", "attribute_fields", "attribute_field", "attribute",
  "ice_option_list", "candidate_list", "candidate", "rel_addr", "rel_port",
  "extension_list", "extension", "url_list", "id_list", "type_list",
  "key_field", "key_type", "time_list", "time", "repeat_list", "repeat",
  "offsets", "timezones_field", "timezone_list", "timezone", "typed_time",
  "time_field", "bandwidth_list", "bandwidth_field", "connection_fields",
  "connection_field_lonely", "connection_field", "conn_address",
  "phone_fields", "phone_field", "phone_number", "phone", "email_fields",
  "email_field", "email_address", "uri_field", "information_field",
  "session_name_field", "proto_version", "origin_field", "email_text",
  "alpha_numeric", "digit", "pos_digit", "email_byte", 0
};
#endif

# ifdef YYPRINT
/* YYTOKNUM[YYLEX-NUM] -- Internal token number corresponding to
   token YYLEX-NUM.  */
static const yytype_uint16 yytoknum[] =
{
       0,   256,   257,   258,   259,   260,   261,   262,   263,   264,
     265,   266,   267,   268,   269,   270,   271,   272,   273,   274,
     275,   276,   277,   278,   279,   280,   281,   282,   283,   284,
     285,   286,   287,   288,   289,   290,   291,   292,   293,   294,
     295,   296,   297,   298,   299,   300,   301,   302,   303,   304,
     305,   306,   307,   308,   309,   310,   311,   312,   313,   314,
     315,   316,   317,   318,   319,   320,   321,   322,   323,   324,
     325,   326,   327,   328,   329,   330,   331,   332,   333,   334,
     335,   336,   337,   338,   339,   340,   341,   342,   343,   344,
     345,   346,   347,   348,   349,   350,   351,   352,   353,   354,
     355,   356,   357,   358,   359,   360,   361,   362,   363,   364,
     365,   366,   367,   368,   369,   370,   371,   372
};
# endif

/* YYR1[YYN] -- Symbol number of symbol that rule YYN derives.  */
static const yytype_uint8 yyr1[] =
{
       0,   118,   119,   120,   120,   121,   122,   122,   123,   123,
     124,   124,   125,   125,   126,   127,   127,   127,   127,   127,
     127,   127,   127,   127,   127,   127,   127,   127,   127,   127,
     127,   127,   127,   127,   127,   127,   127,   127,   127,   127,
     127,   127,   127,   127,   127,   127,   127,   127,   127,   127,
     127,   127,   127,   127,   127,   127,   127,   127,   127,   127,
     127,   127,   127,   127,   127,   127,   127,   127,   127,   127,
     127,   127,   127,   127,   127,   127,   127,   127,   127,   127,
     127,   127,   127,   127,   127,   127,   127,   127,   127,   127,
     127,   128,   128,   129,   129,   130,   131,   131,   132,   132,
     133,   133,   134,   135,   135,   136,   136,   137,   137,   138,
     138,   139,   139,   140,   140,   141,   142,   142,   143,   144,
     144,   145,   145,   146,   146,   147,   148,   148,   149,   150,
     150,   151,   152,   152,   153,   153,   154,   155,   155,   155,
     156,   156,   157,   158,   158,   158,   159,   160,   160,   161,
     162,   162,   162,   162,   163,   163,   164,   164,   165,   166,
     167,   168,   168,   169,   169,   170,   170,   171,   171,   171,
     171,   171,   171,   171,   171,   171,   172,   172,   172,   172,
     172,   172,   172,   172,   172
};

/* YYR2[YYN] -- Number of symbols composing right hand side of rule YYN.  */
static const yytype_uint8 yyr2[] =
{
       0,     2,    14,     0,     2,     6,    11,     9,     1,     3,
       1,     3,     0,     2,     3,     2,     2,     2,     2,     1,
       1,     1,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       4,     2,     2,     4,     2,     2,     1,     1,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
      19,     1,     1,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     4,     2,
       6,     2,     2,     2,     2,     2,     2,     2,     2,     3,
       1,     1,     3,     1,     3,     5,     0,     4,     0,     4,
       0,     2,     4,     1,     3,     1,     3,     1,     3,     0,
       3,     1,     3,     1,     2,     2,     0,     2,     7,     1,
       3,     0,     3,     1,     3,     3,     1,     2,     5,     0,
       2,     5,     0,     2,     0,     7,     7,     1,     3,     5,
       0,     2,     3,     1,     4,     4,     1,     0,     2,     3,
       1,     5,     5,     3,     0,     3,     0,     3,     3,     3,
      13,     1,     2,     1,     1,     1,     1,     1,     1,     1,
       1,     1,     1,     1,     1,     1,     1,     1,     1,     1,
       1,     1,     1,     1,     1
};

/* YYDEFACT[STATE-NAME] -- Default reduction number in state STATE-NUM.
   Performed when YYTABLE doesn't specify something else to do.  Zero
   means the default is an error.  */
static const yytype_uint8 yydefact[] =
{
       0,     0,     0,     0,     0,     1,     0,     0,   159,     0,
       0,   156,     0,     0,     0,   154,     0,   158,     0,     0,
     147,     0,   157,     0,   140,     0,   155,     0,   134,   148,
       0,   165,   167,   168,   169,   170,   171,   172,   173,   174,
     175,   163,   184,   177,   178,   183,     0,   180,   179,   181,
     182,   150,     0,     0,   176,   164,   166,   161,     0,     0,
     129,   141,     0,     0,     0,   149,   184,   162,     0,   143,
     146,     0,     0,     0,   153,     0,     0,   142,     0,     0,
       0,     0,     0,   121,   113,   116,   130,     0,     0,     0,
       0,     0,     0,     0,     0,     0,   114,   109,   115,     0,
     151,   152,   144,   145,     0,     0,     0,     0,     0,   123,
       0,    12,     0,   117,     0,   137,     0,     0,     0,     0,
     122,     0,   111,     0,     3,   126,     0,   160,     0,   135,
     131,   128,   125,   124,     0,   110,     0,     2,    13,   127,
       0,   138,   112,    90,     0,     0,     0,     0,    19,    20,
      21,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,    46,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,    61,    62,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,    47,     0,     0,     4,   156,     0,
       0,     0,    15,    16,    17,    18,    22,    23,    24,    25,
      26,    27,    28,    29,    30,    31,    32,    33,    34,    35,
     103,    36,   107,    37,    38,    39,    41,    42,    44,    45,
      48,    49,    50,    51,    52,    53,    54,    55,    56,    57,
      58,    59,     0,     0,    63,    93,    91,    64,    65,    66,
      67,    68,    69,    70,    71,    72,    73,    74,    75,    76,
      77,    79,     0,    81,    82,    83,    84,    85,    86,    87,
      88,    14,     0,   132,     0,   139,    89,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,   129,     0,
     119,   104,   108,    40,   105,    43,     0,     0,    94,    92,
      78,     0,     0,     0,   109,   133,   118,     0,     0,     0,
       0,     0,     0,     0,     0,    12,   120,   106,     0,    95,
      80,     8,     0,     0,     0,     5,     0,     0,     0,     0,
       0,     0,    10,     0,     9,     0,     0,     0,     7,     0,
       0,     0,     0,    11,     0,   136,     0,     6,     0,     0,
       0,     0,    96,     0,    98,     0,     0,   100,     0,     0,
      60,    97,     0,     0,   101,    99,     0,     0,   102
};

/* YYDEFGOTO[NTERM-NUM].  */
static const yytype_int16 yydefgoto[] =
{
      -1,     2,   137,   217,   218,   342,   353,   124,   138,   215,
     267,   264,   265,   374,   377,   380,   384,   241,   315,   243,
     111,   123,    83,    84,    98,   113,   309,    97,   108,   109,
     126,    85,    72,    86,   308,    60,   325,   116,    28,    61,
      68,    69,    24,    29,    52,    20,    15,    11,     3,     7,
      70,    54,    55,    56,    57
};

/* YYPACT[STATE-NUM] -- Index in YYTABLE of the portion describing
   STATE-NUM.  */
#define YYPACT_NINF -120
static const yytype_int16 yypact[] =
{
     -24,    76,   124,    94,   113,  -120,   100,    95,  -120,   115,
     104,   101,   105,   122,   109,   102,   123,  -120,   126,   112,
    -120,   114,  -120,   130,   120,   146,  -120,    92,   -27,  -120,
     135,  -120,  -120,  -120,  -120,  -120,  -120,  -120,  -120,  -120,
    -120,  -120,  -120,  -120,  -120,  -120,   137,  -120,  -120,  -120,
    -120,   154,   157,   206,  -120,  -120,  -120,  -120,   230,   143,
    -120,  -120,   158,   150,   155,  -120,   165,  -120,   179,    -4,
     230,   180,   -33,   166,  -120,   230,   174,  -120,   230,   230,
     172,   175,   176,   -22,  -120,  -120,  -120,   187,   142,   183,
     173,   184,   192,   193,   210,   197,  -120,   199,   185,   221,
    -120,  -120,  -120,  -120,   222,   223,   224,   212,     3,  -120,
     229,  -120,   231,  -120,   245,   234,   248,   249,   250,   231,
    -120,   197,   238,   252,   225,   253,   251,  -120,   240,  -120,
    -120,  -120,  -120,  -120,   241,  -120,   -25,   226,  -120,  -120,
     231,   246,  -120,   247,   254,   255,   256,   257,  -120,  -120,
    -120,   258,   259,   260,   261,   262,   263,   264,   265,   266,
     267,   268,   269,   270,   271,   272,   273,   273,   274,   275,
     276,   277,   278,  -120,   279,   280,   281,   282,   283,   284,
     285,   286,   287,   288,   289,   290,   291,  -120,  -120,   292,
     293,   294,   295,   296,   297,   298,   299,   300,   301,   302,
     303,   304,   305,   306,   307,   308,   309,   310,   311,   312,
     313,   314,   315,   316,  -120,   332,   318,  -120,   101,   333,
     319,   321,  -120,  -120,  -120,  -120,  -120,  -120,  -120,  -120,
    -120,  -120,  -120,  -120,  -120,  -120,  -120,  -120,  -120,  -120,
    -120,   335,  -120,   336,   336,   337,  -120,   338,  -120,  -120,
    -120,  -120,  -120,  -120,  -120,  -120,  -120,  -120,  -120,  -120,
    -120,  -120,   340,   341,   342,  -120,  -120,   343,  -120,  -120,
    -120,  -120,  -120,  -120,  -120,  -120,  -120,  -120,  -120,  -120,
     344,  -120,   345,  -120,  -120,  -120,  -120,  -120,  -120,  -120,
    -120,  -120,   346,  -120,   231,  -120,  -120,   324,   334,   339,
     347,   348,   349,   292,   350,   351,   352,   353,   237,    99,
    -120,  -120,  -120,  -120,  -120,   354,   355,   356,  -120,  -120,
    -120,   357,   -14,   358,   -31,  -120,  -120,   231,   359,   360,
     361,   362,   363,   364,   367,  -120,  -120,  -120,   368,  -120,
    -120,  -120,   -13,   369,   365,   225,   366,   370,   371,   363,
     380,   381,  -120,   107,  -120,   -11,   222,   372,  -120,   373,
     370,   384,   387,  -120,   108,  -120,   375,  -120,   389,   377,
     391,   379,   393,   382,   394,   396,   385,  -120,   386,   397,
     400,  -120,   388,   390,  -120,  -120,   402,   392,  -120
};

/* YYPGOTO[NTERM-NUM].  */
static const yytype_int16 yypgoto[] =
{
    -120,  -120,  -120,  -120,  -120,   -74,   -84,   -58,  -120,  -120,
    -120,  -120,   -23,  -120,  -120,  -120,  -120,  -120,  -120,   111,
     -45,  -120,  -120,   198,  -120,  -120,  -120,  -120,  -120,   242,
    -119,  -120,    56,  -120,  -120,  -120,  -120,     9,  -120,  -120,
    -120,   374,  -120,  -120,  -120,  -120,   148,  -120,  -120,  -120,
      66,  -120,  -120,  -120,    41
};

/* YYTABLE[YYPACT[STATE-NUM]].  What to do in state STATE-NUM.  If
   positive, shift that token.  If negative, reduce the rule which
   number is the opposite.  If YYTABLE_NINF, syntax error.  */
#define YYTABLE_NINF -1
static const yytype_uint16 yytable[] =
{
     132,   332,   347,   143,   360,    81,     1,    81,    82,    58,
      59,   333,   348,   110,   348,    78,   120,    79,   121,    82,
      95,   219,   144,   145,   146,   147,   148,   149,   150,   151,
     152,   153,   154,   155,   156,   157,   158,   159,   160,   161,
     162,   163,   164,   165,   166,   167,   168,   169,   170,   171,
     172,   173,   174,   175,   176,   177,   178,   179,   180,   181,
     182,   183,   184,   185,   186,   187,   188,   189,   190,   191,
     192,   193,   194,   195,   196,   197,   198,   199,   200,   201,
     202,   203,   204,   205,   206,   207,   208,   209,   210,   211,
     212,   213,   214,    53,    67,    31,    32,    33,    34,    35,
      36,    37,    38,    39,    40,     4,    41,    42,    43,    44,
      45,    67,   326,    46,   327,    47,    48,    49,    50,    51,
     358,   367,   359,   359,     5,     6,     8,    10,     9,    67,
      12,    67,    13,    16,    14,    17,    19,    18,    21,    22,
      23,    88,    25,    26,    90,    31,    32,    33,    34,    35,
      36,    37,    38,    39,    40,    27,    41,    42,    43,    44,
      45,    30,   100,    62,    63,    47,    48,    49,    50,    64,
      65,    71,    74,    73,    75,   310,    31,    32,    33,    34,
      35,    36,    37,    38,    39,    40,    76,    41,    42,    43,
      44,    45,    77,   102,    87,    80,    47,    48,    49,    50,
      92,    89,    99,    93,    94,   101,   103,   104,   336,    31,
      32,    33,    34,    35,    36,    37,    38,    39,    40,   105,
      41,    66,    43,    44,    45,   106,   107,   119,   112,    47,
      48,    49,    50,    31,    32,    33,    34,    35,    36,    37,
      38,    39,    40,   110,    41,    42,    43,    44,    45,   114,
     115,   117,   118,    47,    48,    49,    50,   122,   127,   128,
     125,   129,   130,   131,   134,   135,   140,   139,   141,   142,
     136,   220,   216,   221,   323,   355,   364,   345,   244,   335,
     318,    96,   222,   223,   224,   225,   226,   227,   228,   229,
     230,   231,   232,   233,   234,   235,   236,   237,   238,   239,
     240,   242,   245,   246,   247,   248,   249,   250,   251,   252,
     253,   254,   255,   256,   257,   258,   259,   260,   261,   262,
     263,   266,   268,   269,   270,   271,   272,   273,   274,   275,
     276,   277,   278,   279,   280,   281,   282,   283,   284,   285,
     286,   287,   288,   289,   290,   291,   292,   295,   294,   296,
     297,   298,   311,   300,   299,   301,   302,   303,   304,   305,
     306,   307,   312,   133,   324,   361,   293,   313,     0,   328,
     329,   330,   331,     0,     0,   314,   316,   317,   319,   320,
     321,   322,   344,   346,   349,     0,   334,   337,   338,   339,
     340,   341,   343,   350,   351,   356,   357,   365,   352,   354,
     362,   363,   366,   368,   369,   370,   371,   372,   373,   376,
     375,   378,   382,   379,   381,   383,   385,   387,   386,     0,
     388,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,    91
};

#define yypact_value_is_default(yystate) \
  ((yystate) == (-120))

#define yytable_value_is_error(yytable_value) \
  YYID (0)

static const yytype_int16 yycheck[] =
{
     119,    15,    15,    28,    15,    38,    30,    38,    41,    36,
      37,    25,    25,    44,    25,    19,    13,    21,    15,    41,
      42,   140,    47,    48,    49,    50,    51,    52,    53,    54,
      55,    56,    57,    58,    59,    60,    61,    62,    63,    64,
      65,    66,    67,    68,    69,    70,    71,    72,    73,    74,
      75,    76,    77,    78,    79,    80,    81,    82,    83,    84,
      85,    86,    87,    88,    89,    90,    91,    92,    93,    94,
      95,    96,    97,    98,    99,   100,   101,   102,   103,   104,
     105,   106,   107,   108,   109,   110,   111,   112,   113,   114,
     115,   116,   117,    27,    53,     3,     4,     5,     6,     7,
       8,     9,    10,    11,    12,    29,    14,    15,    16,    17,
      18,    70,    13,    21,    15,    23,    24,    25,    26,    27,
      13,    13,    15,    15,     0,    31,    13,    32,    28,    88,
      15,    90,    28,    28,    33,    13,    34,    28,    15,    13,
      28,    75,    28,    13,    78,     3,     4,     5,     6,     7,
       8,     9,    10,    11,    12,    35,    14,    15,    16,    17,
      18,    15,    20,    28,    27,    23,    24,    25,    26,    15,
      13,    28,    22,    15,    19,   294,     3,     4,     5,     6,
       7,     8,     9,    10,    11,    12,    21,    14,    15,    16,
      17,    18,    13,    20,    28,    15,    23,    24,    25,    26,
      28,    27,    15,    28,    28,    22,    22,    15,   327,     3,
       4,     5,     6,     7,     8,     9,    10,    11,    12,    26,
      14,    15,    16,    17,    18,    15,    29,    15,    43,    23,
      24,    25,    26,     3,     4,     5,     6,     7,     8,     9,
      10,    11,    12,    44,    14,    15,    16,    17,    18,    28,
      28,    28,    28,    23,    24,    25,    26,    28,    13,    25,
      29,    13,    13,    13,    26,    13,    15,    14,    28,    28,
      45,    25,    46,    26,    37,   349,   360,   335,   167,   324,
     303,    83,    28,    28,    28,    28,    28,    28,    28,    28,
      28,    28,    28,    28,    28,    28,    28,    28,    28,    28,
      28,    28,    28,    28,    28,    28,    28,    28,    28,    28,
      28,    28,    28,    28,    28,    28,    28,    28,    28,    28,
      28,    28,    28,    28,    28,    28,    28,    28,    28,    28,
      28,    28,    28,    28,    28,    28,    28,    28,    28,    28,
      28,    28,    28,    28,    28,    13,    28,    28,    15,    28,
      15,    15,    28,    15,    17,    15,    15,    15,    15,    15,
      15,    15,    28,   121,   308,   356,   218,    28,    -1,    15,
      15,    15,    15,    -1,    -1,    28,    28,    28,    28,    28,
      28,    28,    15,    15,    15,    -1,    28,    28,    28,    28,
      28,    28,    28,    28,    28,    15,    15,    13,    28,    28,
      28,    28,    15,    28,    15,    28,    15,    28,    15,    15,
      28,    15,    15,    28,    28,    15,    28,    15,    28,    -1,
      28,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    79
};

/* YYSTOS[STATE-NUM] -- The (internal number of the) accessing
   symbol of state STATE-NUM.  */
static const yytype_uint8 yystos[] =
{
       0,    30,   119,   166,    29,     0,    31,   167,    13,    28,
      32,   165,    15,    28,    33,   164,    28,    13,    28,    34,
     163,    15,    13,    28,   160,    28,    13,    35,   156,   161,
      15,     3,     4,     5,     6,     7,     8,     9,    10,    11,
      12,    14,    15,    16,    17,    18,    21,    23,    24,    25,
      26,    27,   162,   168,   169,   170,   171,   172,    36,    37,
     153,   157,    28,    27,    15,    13,    15,   172,   158,   159,
     168,    28,   150,    15,    22,    19,    21,    13,    19,    21,
      15,    38,    41,   140,   141,   149,   151,    28,   168,    27,
     168,   159,    28,    28,    28,    42,   141,   145,   142,    15,
      20,    22,    20,    22,    15,    26,    15,    29,   146,   147,
      44,   138,    43,   143,    28,    28,   155,    28,    28,    15,
      13,    15,    28,   139,   125,    29,   148,    13,    25,    13,
      13,    13,   148,   147,    26,    13,    45,   120,   126,    14,
      15,    28,    28,    28,    47,    48,    49,    50,    51,    52,
      53,    54,    55,    56,    57,    58,    59,    60,    61,    62,
      63,    64,    65,    66,    67,    68,    69,    70,    71,    72,
      73,    74,    75,    76,    77,    78,    79,    80,    81,    82,
      83,    84,    85,    86,    87,    88,    89,    90,    91,    92,
      93,    94,    95,    96,    97,    98,    99,   100,   101,   102,
     103,   104,   105,   106,   107,   108,   109,   110,   111,   112,
     113,   114,   115,   116,   117,   127,    46,   121,   122,   148,
      25,    26,    28,    28,    28,    28,    28,    28,    28,    28,
      28,    28,    28,    28,    28,    28,    28,    28,    28,    28,
      28,   135,    28,   137,   137,    28,    28,    28,    28,    28,
      28,    28,    28,    28,    28,    28,    28,    28,    28,    28,
      28,    28,    28,    28,   129,   130,    28,   128,    28,    28,
      28,    28,    28,    28,    28,    28,    28,    28,    28,    28,
      28,    28,    28,    28,    28,    28,    28,    28,    28,    28,
      28,    13,    28,   164,    15,    28,    28,    15,    15,    17,
      15,    15,    15,    15,    15,    15,    15,    15,   152,   144,
     148,    28,    28,    28,    28,   136,    28,    28,   130,    28,
      28,    28,    28,    37,   150,   154,    13,    15,    15,    15,
      15,    15,    15,    25,    28,   138,   148,    28,    28,    28,
      28,    28,   123,    28,    15,   125,    15,    15,    25,    15,
      28,    28,    28,   124,    28,   123,    15,    15,    13,    15,
      15,   155,    28,    28,   124,    13,    15,    13,    28,    15,
      28,    15,    28,    15,   131,    28,    15,   132,    15,    28,
     133,    28,    15,    15,   134,    28,    28,    15,    28
};

#define yyerrok		(yyerrstatus = 0)
#define yyclearin	(yychar = YYEMPTY)
#define YYEMPTY		(-2)
#define YYEOF		0

#define YYACCEPT	goto yyacceptlab
#define YYABORT		goto yyabortlab
#define YYERROR		goto yyerrorlab


/* Like YYERROR except do call yyerror.  This remains here temporarily
   to ease the transition to the new meaning of YYERROR, for GCC.
   Once GCC version 2 has supplanted version 1, this can go.  However,
   YYFAIL appears to be in use.  Nevertheless, it is formally deprecated
   in Bison 2.4.2's NEWS entry, where a plan to phase it out is
   discussed.  */

#define YYFAIL		goto yyerrlab
#if defined YYFAIL
  /* This is here to suppress warnings from the GCC cpp's
     -Wunused-macros.  Normally we don't worry about that warning, but
     some users do, and we want to make it easy for users to remove
     YYFAIL uses, which will produce warnings from Bison 2.5.  */
#endif

#define YYRECOVERING()  (!!yyerrstatus)

#define YYBACKUP(Token, Value)					\
do								\
  if (yychar == YYEMPTY && yylen == 1)				\
    {								\
      yychar = (Token);						\
      yylval = (Value);						\
      YYPOPSTACK (1);						\
      goto yybackup;						\
    }								\
  else								\
    {								\
      yyerror (YY_("syntax error: cannot back up")); \
      YYERROR;							\
    }								\
while (YYID (0))


#define YYTERROR	1
#define YYERRCODE	256


/* YYLLOC_DEFAULT -- Set CURRENT to span from RHS[1] to RHS[N].
   If N is 0, then set CURRENT to the empty location which ends
   the previous symbol: RHS[0] (always defined).  */

#define YYRHSLOC(Rhs, K) ((Rhs)[K])
#ifndef YYLLOC_DEFAULT
# define YYLLOC_DEFAULT(Current, Rhs, N)				\
    do									\
      if (YYID (N))                                                    \
	{								\
	  (Current).first_line   = YYRHSLOC (Rhs, 1).first_line;	\
	  (Current).first_column = YYRHSLOC (Rhs, 1).first_column;	\
	  (Current).last_line    = YYRHSLOC (Rhs, N).last_line;		\
	  (Current).last_column  = YYRHSLOC (Rhs, N).last_column;	\
	}								\
      else								\
	{								\
	  (Current).first_line   = (Current).last_line   =		\
	    YYRHSLOC (Rhs, 0).last_line;				\
	  (Current).first_column = (Current).last_column =		\
	    YYRHSLOC (Rhs, 0).last_column;				\
	}								\
    while (YYID (0))
#endif


/* This macro is provided for backward compatibility. */

#ifndef YY_LOCATION_PRINT
# define YY_LOCATION_PRINT(File, Loc) ((void) 0)
#endif


/* YYLEX -- calling `yylex' with the right arguments.  */

#ifdef YYLEX_PARAM
# define YYLEX yylex (YYLEX_PARAM)
#else
# define YYLEX yylex ()
#endif

/* Enable debugging if requested.  */
#if YYDEBUG

# ifndef YYFPRINTF
#  include <stdio.h> /* INFRINGES ON USER NAME SPACE */
#  define YYFPRINTF fprintf
# endif

# define YYDPRINTF(Args)			\
do {						\
  if (yydebug)					\
    YYFPRINTF Args;				\
} while (YYID (0))

# define YY_SYMBOL_PRINT(Title, Type, Value, Location)			  \
do {									  \
  if (yydebug)								  \
    {									  \
      YYFPRINTF (stderr, "%s ", Title);					  \
      yy_symbol_print (stderr,						  \
		  Type, Value); \
      YYFPRINTF (stderr, "\n");						  \
    }									  \
} while (YYID (0))


/*--------------------------------.
| Print this symbol on YYOUTPUT.  |
`--------------------------------*/

/*ARGSUSED*/
#if (defined __STDC__ || defined __C99__FUNC__ \
     || defined __cplusplus || defined _MSC_VER)
static void
yy_symbol_value_print (FILE *yyoutput, int yytype, YYSTYPE const * const yyvaluep)
#else
static void
yy_symbol_value_print (yyoutput, yytype, yyvaluep)
    FILE *yyoutput;
    int yytype;
    YYSTYPE const * const yyvaluep;
#endif
{
  if (!yyvaluep)
    return;
# ifdef YYPRINT
  if (yytype < YYNTOKENS)
    YYPRINT (yyoutput, yytoknum[yytype], *yyvaluep);
# else
  YYUSE (yyoutput);
# endif
  switch (yytype)
    {
      default:
	break;
    }
}


/*--------------------------------.
| Print this symbol on YYOUTPUT.  |
`--------------------------------*/

#if (defined __STDC__ || defined __C99__FUNC__ \
     || defined __cplusplus || defined _MSC_VER)
static void
yy_symbol_print (FILE *yyoutput, int yytype, YYSTYPE const * const yyvaluep)
#else
static void
yy_symbol_print (yyoutput, yytype, yyvaluep)
    FILE *yyoutput;
    int yytype;
    YYSTYPE const * const yyvaluep;
#endif
{
  if (yytype < YYNTOKENS)
    YYFPRINTF (yyoutput, "token %s (", yytname[yytype]);
  else
    YYFPRINTF (yyoutput, "nterm %s (", yytname[yytype]);

  yy_symbol_value_print (yyoutput, yytype, yyvaluep);
  YYFPRINTF (yyoutput, ")");
}

/*------------------------------------------------------------------.
| yy_stack_print -- Print the state stack from its BOTTOM up to its |
| TOP (included).                                                   |
`------------------------------------------------------------------*/

#if (defined __STDC__ || defined __C99__FUNC__ \
     || defined __cplusplus || defined _MSC_VER)
static void
yy_stack_print (yytype_int16 *yybottom, yytype_int16 *yytop)
#else
static void
yy_stack_print (yybottom, yytop)
    yytype_int16 *yybottom;
    yytype_int16 *yytop;
#endif
{
  YYFPRINTF (stderr, "Stack now");
  for (; yybottom <= yytop; yybottom++)
    {
      int yybot = *yybottom;
      YYFPRINTF (stderr, " %d", yybot);
    }
  YYFPRINTF (stderr, "\n");
}

# define YY_STACK_PRINT(Bottom, Top)				\
do {								\
  if (yydebug)							\
    yy_stack_print ((Bottom), (Top));				\
} while (YYID (0))


/*------------------------------------------------.
| Report that the YYRULE is going to be reduced.  |
`------------------------------------------------*/

#if (defined __STDC__ || defined __C99__FUNC__ \
     || defined __cplusplus || defined _MSC_VER)
static void
yy_reduce_print (YYSTYPE *yyvsp, int yyrule)
#else
static void
yy_reduce_print (yyvsp, yyrule)
    YYSTYPE *yyvsp;
    int yyrule;
#endif
{
  int yynrhs = yyr2[yyrule];
  int yyi;
  unsigned long int yylno = yyrline[yyrule];
  YYFPRINTF (stderr, "Reducing stack by rule %d (line %lu):\n",
	     yyrule - 1, yylno);
  /* The symbols being reduced.  */
  for (yyi = 0; yyi < yynrhs; yyi++)
    {
      YYFPRINTF (stderr, "   $%d = ", yyi + 1);
      yy_symbol_print (stderr, yyrhs[yyprhs[yyrule] + yyi],
		       &(yyvsp[(yyi + 1) - (yynrhs)])
		       		       );
      YYFPRINTF (stderr, "\n");
    }
}

# define YY_REDUCE_PRINT(Rule)		\
do {					\
  if (yydebug)				\
    yy_reduce_print (yyvsp, Rule); \
} while (YYID (0))

/* Nonzero means print parse trace.  It is left uninitialized so that
   multiple parsers can coexist.  */
int yydebug;
#else /* !YYDEBUG */
# define YYDPRINTF(Args)
# define YY_SYMBOL_PRINT(Title, Type, Value, Location)
# define YY_STACK_PRINT(Bottom, Top)
# define YY_REDUCE_PRINT(Rule)
#endif /* !YYDEBUG */


/* YYINITDEPTH -- initial size of the parser's stacks.  */
#ifndef	YYINITDEPTH
# define YYINITDEPTH 200
#endif

/* YYMAXDEPTH -- maximum size the stacks can grow to (effective only
   if the built-in stack extension method is used).

   Do not make this value too large; the results are undefined if
   YYSTACK_ALLOC_MAXIMUM < YYSTACK_BYTES (YYMAXDEPTH)
   evaluated with infinite-precision integer arithmetic.  */

#ifndef YYMAXDEPTH
# define YYMAXDEPTH 10000
#endif


#if YYERROR_VERBOSE

# ifndef yystrlen
#  if defined __GLIBC__ && defined _STRING_H
#   define yystrlen strlen
#  else
/* Return the length of YYSTR.  */
#if (defined __STDC__ || defined __C99__FUNC__ \
     || defined __cplusplus || defined _MSC_VER)
static YYSIZE_T
yystrlen (const char *yystr)
#else
static YYSIZE_T
yystrlen (yystr)
    const char *yystr;
#endif
{
  YYSIZE_T yylen;
  for (yylen = 0; yystr[yylen]; yylen++)
    continue;
  return yylen;
}
#  endif
# endif

# ifndef yystpcpy
#  if defined __GLIBC__ && defined _STRING_H && defined _GNU_SOURCE
#   define yystpcpy stpcpy
#  else
/* Copy YYSRC to YYDEST, returning the address of the terminating '\0' in
   YYDEST.  */
#if (defined __STDC__ || defined __C99__FUNC__ \
     || defined __cplusplus || defined _MSC_VER)
static char *
yystpcpy (char *yydest, const char *yysrc)
#else
static char *
yystpcpy (yydest, yysrc)
    char *yydest;
    const char *yysrc;
#endif
{
  char *yyd = yydest;
  const char *yys = yysrc;

  while ((*yyd++ = *yys++) != '\0')
    continue;

  return yyd - 1;
}
#  endif
# endif

# ifndef yytnamerr
/* Copy to YYRES the contents of YYSTR after stripping away unnecessary
   quotes and backslashes, so that it's suitable for yyerror.  The
   heuristic is that double-quoting is unnecessary unless the string
   contains an apostrophe, a comma, or backslash (other than
   backslash-backslash).  YYSTR is taken from yytname.  If YYRES is
   null, do not copy; instead, return the length of what the result
   would have been.  */
static YYSIZE_T
yytnamerr (char *yyres, const char *yystr)
{
  if (*yystr == '"')
    {
      YYSIZE_T yyn = 0;
      char const *yyp = yystr;

      for (;;)
	switch (*++yyp)
	  {
	  case '\'':
	  case ',':
	    goto do_not_strip_quotes;

	  case '\\':
	    if (*++yyp != '\\')
	      goto do_not_strip_quotes;
	    /* Fall through.  */
	  default:
	    if (yyres)
	      yyres[yyn] = *yyp;
	    yyn++;
	    break;

	  case '"':
	    if (yyres)
	      yyres[yyn] = '\0';
	    return yyn;
	  }
    do_not_strip_quotes: ;
    }

  if (! yyres)
    return yystrlen (yystr);

  return yystpcpy (yyres, yystr) - yyres;
}
# endif

/* Copy into *YYMSG, which is of size *YYMSG_ALLOC, an error message
   about the unexpected token YYTOKEN for the state stack whose top is
   YYSSP.

   Return 0 if *YYMSG was successfully written.  Return 1 if *YYMSG is
   not large enough to hold the message.  In that case, also set
   *YYMSG_ALLOC to the required number of bytes.  Return 2 if the
   required number of bytes is too large to store.  */
static int
yysyntax_error (YYSIZE_T *yymsg_alloc, char **yymsg,
                yytype_int16 *yyssp, int yytoken)
{
  YYSIZE_T yysize0 = yytnamerr (0, yytname[yytoken]);
  YYSIZE_T yysize = yysize0;
  YYSIZE_T yysize1;
  enum { YYERROR_VERBOSE_ARGS_MAXIMUM = 5 };
  /* Internationalized format string. */
  const char *yyformat = 0;
  /* Arguments of yyformat. */
  char const *yyarg[YYERROR_VERBOSE_ARGS_MAXIMUM];
  /* Number of reported tokens (one for the "unexpected", one per
     "expected"). */
  int yycount = 0;

  /* There are many possibilities here to consider:
     - Assume YYFAIL is not used.  It's too flawed to consider.  See
       <http://lists.gnu.org/archive/html/bison-patches/2009-12/msg00024.html>
       for details.  YYERROR is fine as it does not invoke this
       function.
     - If this state is a consistent state with a default action, then
       the only way this function was invoked is if the default action
       is an error action.  In that case, don't check for expected
       tokens because there are none.
     - The only way there can be no lookahead present (in yychar) is if
       this state is a consistent state with a default action.  Thus,
       detecting the absence of a lookahead is sufficient to determine
       that there is no unexpected or expected token to report.  In that
       case, just report a simple "syntax error".
     - Don't assume there isn't a lookahead just because this state is a
       consistent state with a default action.  There might have been a
       previous inconsistent state, consistent state with a non-default
       action, or user semantic action that manipulated yychar.
     - Of course, the expected token list depends on states to have
       correct lookahead information, and it depends on the parser not
       to perform extra reductions after fetching a lookahead from the
       scanner and before detecting a syntax error.  Thus, state merging
       (from LALR or IELR) and default reductions corrupt the expected
       token list.  However, the list is correct for canonical LR with
       one exception: it will still contain any token that will not be
       accepted due to an error action in a later state.
  */
  if (yytoken != YYEMPTY)
    {
      int yyn = yypact[*yyssp];
      yyarg[yycount++] = yytname[yytoken];
      if (!yypact_value_is_default (yyn))
        {
          /* Start YYX at -YYN if negative to avoid negative indexes in
             YYCHECK.  In other words, skip the first -YYN actions for
             this state because they are default actions.  */
          int yyxbegin = yyn < 0 ? -yyn : 0;
          /* Stay within bounds of both yycheck and yytname.  */
          int yychecklim = YYLAST - yyn + 1;
          int yyxend = yychecklim < YYNTOKENS ? yychecklim : YYNTOKENS;
          int yyx;

          for (yyx = yyxbegin; yyx < yyxend; ++yyx)
            if (yycheck[yyx + yyn] == yyx && yyx != YYTERROR
                && !yytable_value_is_error (yytable[yyx + yyn]))
              {
                if (yycount == YYERROR_VERBOSE_ARGS_MAXIMUM)
                  {
                    yycount = 1;
                    yysize = yysize0;
                    break;
                  }
                yyarg[yycount++] = yytname[yyx];
                yysize1 = yysize + yytnamerr (0, yytname[yyx]);
                if (! (yysize <= yysize1
                       && yysize1 <= YYSTACK_ALLOC_MAXIMUM))
                  return 2;
                yysize = yysize1;
              }
        }
    }

  switch (yycount)
    {
# define YYCASE_(N, S)                      \
      case N:                               \
        yyformat = S;                       \
      break
      YYCASE_(0, YY_("syntax error"));
      YYCASE_(1, YY_("syntax error, unexpected %s"));
      YYCASE_(2, YY_("syntax error, unexpected %s, expecting %s"));
      YYCASE_(3, YY_("syntax error, unexpected %s, expecting %s or %s"));
      YYCASE_(4, YY_("syntax error, unexpected %s, expecting %s or %s or %s"));
      YYCASE_(5, YY_("syntax error, unexpected %s, expecting %s or %s or %s or %s"));
# undef YYCASE_
    }

  yysize1 = yysize + yystrlen (yyformat);
  if (! (yysize <= yysize1 && yysize1 <= YYSTACK_ALLOC_MAXIMUM))
    return 2;
  yysize = yysize1;

  if (*yymsg_alloc < yysize)
    {
      *yymsg_alloc = 2 * yysize;
      if (! (yysize <= *yymsg_alloc
             && *yymsg_alloc <= YYSTACK_ALLOC_MAXIMUM))
        *yymsg_alloc = YYSTACK_ALLOC_MAXIMUM;
      return 1;
    }

  /* Avoid sprintf, as that infringes on the user's name space.
     Don't have undefined behavior even if the translation
     produced a string with the wrong number of "%s"s.  */
  {
    char *yyp = *yymsg;
    int yyi = 0;
    while ((*yyp = *yyformat) != '\0')
      if (*yyp == '%' && yyformat[1] == 's' && yyi < yycount)
        {
          yyp += yytnamerr (yyp, yyarg[yyi++]);
          yyformat += 2;
        }
      else
        {
          yyp++;
          yyformat++;
        }
  }
  return 0;
}
#endif /* YYERROR_VERBOSE */

/*-----------------------------------------------.
| Release the memory associated to this symbol.  |
`-----------------------------------------------*/

/*ARGSUSED*/
#if (defined __STDC__ || defined __C99__FUNC__ \
     || defined __cplusplus || defined _MSC_VER)
static void
yydestruct (const char *yymsg, int yytype, YYSTYPE *yyvaluep)
#else
static void
yydestruct (yymsg, yytype, yyvaluep)
    const char *yymsg;
    int yytype;
    YYSTYPE *yyvaluep;
#endif
{
  YYUSE (yyvaluep);

  if (!yymsg)
    yymsg = "Deleting";
  YY_SYMBOL_PRINT (yymsg, yytype, yyvaluep, yylocationp);

  switch (yytype)
    {

      default:
	break;
    }
}


/* Prevent warnings from -Wmissing-prototypes.  */
#ifdef YYPARSE_PARAM
#if defined __STDC__ || defined __cplusplus
int yyparse (void *YYPARSE_PARAM);
#else
int yyparse ();
#endif
#else /* ! YYPARSE_PARAM */
#if defined __STDC__ || defined __cplusplus
int yyparse (void);
#else
int yyparse ();
#endif
#endif /* ! YYPARSE_PARAM */


/* The lookahead symbol.  */
int yychar;

/* The semantic value of the lookahead symbol.  */
YYSTYPE yylval;

/* Number of syntax errors so far.  */
int yynerrs;


/*----------.
| yyparse.  |
`----------*/

#ifdef YYPARSE_PARAM
#if (defined __STDC__ || defined __C99__FUNC__ \
     || defined __cplusplus || defined _MSC_VER)
int
yyparse (void *YYPARSE_PARAM)
#else
int
yyparse (YYPARSE_PARAM)
    void *YYPARSE_PARAM;
#endif
#else /* ! YYPARSE_PARAM */
#if (defined __STDC__ || defined __C99__FUNC__ \
     || defined __cplusplus || defined _MSC_VER)
int
yyparse (void)
#else
int
yyparse ()

#endif
#endif
{
    int yystate;
    /* Number of tokens to shift before error messages enabled.  */
    int yyerrstatus;

    /* The stacks and their tools:
       `yyss': related to states.
       `yyvs': related to semantic values.

       Refer to the stacks thru separate pointers, to allow yyoverflow
       to reallocate them elsewhere.  */

    /* The state stack.  */
    yytype_int16 yyssa[YYINITDEPTH];
    yytype_int16 *yyss;
    yytype_int16 *yyssp;

    /* The semantic value stack.  */
    YYSTYPE yyvsa[YYINITDEPTH];
    YYSTYPE *yyvs;
    YYSTYPE *yyvsp;

    YYSIZE_T yystacksize;

  int yyn;
  int yyresult;
  /* Lookahead token as an internal (translated) token number.  */
  int yytoken;
  /* The variables used to return semantic value and location from the
     action routines.  */
  YYSTYPE yyval;

#if YYERROR_VERBOSE
  /* Buffer for error messages, and its allocated size.  */
  char yymsgbuf[128];
  char *yymsg = yymsgbuf;
  YYSIZE_T yymsg_alloc = sizeof yymsgbuf;
#endif

#define YYPOPSTACK(N)   (yyvsp -= (N), yyssp -= (N))

  /* The number of symbols on the RHS of the reduced rule.
     Keep to zero when no symbol should be popped.  */
  int yylen = 0;

  yytoken = 0;
  yyss = yyssa;
  yyvs = yyvsa;
  yystacksize = YYINITDEPTH;

  YYDPRINTF ((stderr, "Starting parse\n"));

  yystate = 0;
  yyerrstatus = 0;
  yynerrs = 0;
  yychar = YYEMPTY; /* Cause a token to be read.  */

  /* Initialize stack pointers.
     Waste one element of value and location stack
     so that they stay on the same level as the state stack.
     The wasted elements are never initialized.  */
  yyssp = yyss;
  yyvsp = yyvs;

  goto yysetstate;

/*------------------------------------------------------------.
| yynewstate -- Push a new state, which is found in yystate.  |
`------------------------------------------------------------*/
 yynewstate:
  /* In all cases, when you get here, the value and location stacks
     have just been pushed.  So pushing a state here evens the stacks.  */
  yyssp++;

 yysetstate:
  *yyssp = yystate;

  if (yyss + yystacksize - 1 <= yyssp)
    {
      /* Get the current used size of the three stacks, in elements.  */
      YYSIZE_T yysize = yyssp - yyss + 1;

#ifdef yyoverflow
      {
	/* Give user a chance to reallocate the stack.  Use copies of
	   these so that the &'s don't force the real ones into
	   memory.  */
	YYSTYPE *yyvs1 = yyvs;
	yytype_int16 *yyss1 = yyss;

	/* Each stack pointer address is followed by the size of the
	   data in use in that stack, in bytes.  This used to be a
	   conditional around just the two extra args, but that might
	   be undefined if yyoverflow is a macro.  */
	yyoverflow (YY_("memory exhausted"),
		    &yyss1, yysize * sizeof (*yyssp),
		    &yyvs1, yysize * sizeof (*yyvsp),
		    &yystacksize);

	yyss = yyss1;
	yyvs = yyvs1;
      }
#else /* no yyoverflow */
# ifndef YYSTACK_RELOCATE
      goto yyexhaustedlab;
# else
      /* Extend the stack our own way.  */
      if (YYMAXDEPTH <= yystacksize)
	goto yyexhaustedlab;
      yystacksize *= 2;
      if (YYMAXDEPTH < yystacksize)
	yystacksize = YYMAXDEPTH;

      {
	yytype_int16 *yyss1 = yyss;
	union yyalloc *yyptr =
	  (union yyalloc *) YYSTACK_ALLOC (YYSTACK_BYTES (yystacksize));
	if (! yyptr)
	  goto yyexhaustedlab;
	YYSTACK_RELOCATE (yyss_alloc, yyss);
	YYSTACK_RELOCATE (yyvs_alloc, yyvs);
#  undef YYSTACK_RELOCATE
	if (yyss1 != yyssa)
	  YYSTACK_FREE (yyss1);
      }
# endif
#endif /* no yyoverflow */

      yyssp = yyss + yysize - 1;
      yyvsp = yyvs + yysize - 1;

      YYDPRINTF ((stderr, "Stack size increased to %lu\n",
		  (unsigned long int) yystacksize));

      if (yyss + yystacksize - 1 <= yyssp)
	YYABORT;
    }

  YYDPRINTF ((stderr, "Entering state %d\n", yystate));

  if (yystate == YYFINAL)
    YYACCEPT;

  goto yybackup;

/*-----------.
| yybackup.  |
`-----------*/
yybackup:

  /* Do appropriate processing given the current state.  Read a
     lookahead token if we need one and don't already have one.  */

  /* First try to decide what to do without reference to lookahead token.  */
  yyn = yypact[yystate];
  if (yypact_value_is_default (yyn))
    goto yydefault;

  /* Not known => get a lookahead token if don't already have one.  */

  /* YYCHAR is either YYEMPTY or YYEOF or a valid lookahead symbol.  */
  if (yychar == YYEMPTY)
    {
      YYDPRINTF ((stderr, "Reading a token: "));
      yychar = YYLEX;
    }

  if (yychar <= YYEOF)
    {
      yychar = yytoken = YYEOF;
      YYDPRINTF ((stderr, "Now at end of input.\n"));
    }
  else
    {
      yytoken = YYTRANSLATE (yychar);
      YY_SYMBOL_PRINT ("Next token is", yytoken, &yylval, &yylloc);
    }

  /* If the proper action on seeing token YYTOKEN is to reduce or to
     detect an error, take that action.  */
  yyn += yytoken;
  if (yyn < 0 || YYLAST < yyn || yycheck[yyn] != yytoken)
    goto yydefault;
  yyn = yytable[yyn];
  if (yyn <= 0)
    {
      if (yytable_value_is_error (yyn))
        goto yyerrlab;
      yyn = -yyn;
      goto yyreduce;
    }

  /* Count tokens shifted since error; after three, turn off error
     status.  */
  if (yyerrstatus)
    yyerrstatus--;

  /* Shift the lookahead token.  */
  YY_SYMBOL_PRINT ("Shifting", yytoken, &yylval, &yylloc);

  /* Discard the shifted token.  */
  yychar = YYEMPTY;

  yystate = yyn;
  *++yyvsp = yylval;

  goto yynewstate;


/*-----------------------------------------------------------.
| yydefault -- do the default action for the current state.  |
`-----------------------------------------------------------*/
yydefault:
  yyn = yydefact[yystate];
  if (yyn == 0)
    goto yyerrlab;
  goto yyreduce;


/*-----------------------------.
| yyreduce -- Do a reduction.  |
`-----------------------------*/
yyreduce:
  /* yyn is the number of a rule to reduce with.  */
  yylen = yyr2[yyn];

  /* If YYLEN is nonzero, implement the default value of the action:
     `$$ = $1'.

     Otherwise, the following line sets YYVAL to garbage.
     This behavior is undocumented and Bison
     users should not rely upon it.  Assigning to YYVAL
     unconditionally makes the parser a bit smaller, and it avoids a
     GCC warning that YYVAL may be used uninitialized.  */
  yyval = yyvsp[1-yylen];


  YY_REDUCE_PRINT (yyn);
  switch (yyn)
    {
        case 2:

/* Line 1806 of yacc.c  */
#line 183 "SDP_parser.y"
    {
//				SDP_parse_parsed_message = new SDP__Message;
				(*SDP_parse_parsed_message).protocol__version() = (yyvsp[(1) - (14)].number);
				(*SDP_parse_parsed_message).origin() = *(yyvsp[(2) - (14)].t_sdp_origin);
				delete (yyvsp[(2) - (14)].t_sdp_origin);
				(*SDP_parse_parsed_message).session__name() = *(yyvsp[(3) - (14)].t_charstring);
				delete (yyvsp[(3) - (14)].t_charstring);

				if ((yyvsp[(4) - (14)].t_charstring) != NULL) { // information field present
					(*SDP_parse_parsed_message).information() = *(yyvsp[(4) - (14)].t_charstring);
					delete (yyvsp[(4) - (14)].t_charstring);
				} else
					(*SDP_parse_parsed_message).information() = OMIT_VALUE;

				if ((yyvsp[(5) - (14)].t_charstring) != NULL) { // uri field present
					(*SDP_parse_parsed_message).uri() = *(yyvsp[(5) - (14)].t_charstring);
					delete (yyvsp[(5) - (14)].t_charstring);
				} else
					(*SDP_parse_parsed_message).uri() = OMIT_VALUE;
				if ((yyvsp[(6) - (14)].t_sdp_email_list) != NULL) { //email fields present
					(*SDP_parse_parsed_message).emails() = *(yyvsp[(6) - (14)].t_sdp_email_list);
					delete (yyvsp[(6) - (14)].t_sdp_email_list);
				} else
					(*SDP_parse_parsed_message).emails() = OMIT_VALUE;
				if ((yyvsp[(7) - (14)].t_sdp_phone_list) != NULL) { // phone fields present
					(*SDP_parse_parsed_message).phone__numbers() = *(yyvsp[(7) - (14)].t_sdp_phone_list);
					delete (yyvsp[(7) - (14)].t_sdp_phone_list);
				} else
					(*SDP_parse_parsed_message).phone__numbers() = OMIT_VALUE;
				
				
				if ((yyvsp[(8) - (14)].t_sdp_connection) != NULL) { // connection fields present
				(*SDP_parse_parsed_message).connection() = (*(yyvsp[(8) - (14)].t_sdp_connection));
				delete (yyvsp[(8) - (14)].t_sdp_connection);
				} else 
				  (*SDP_parse_parsed_message).connection() = OMIT_VALUE;
				  
				if ((yyvsp[(9) - (14)].t_bandwidth_list) != NULL) {
					(*SDP_parse_parsed_message).bandwidth() = *(yyvsp[(9) - (14)].t_bandwidth_list);
					delete (yyvsp[(9) - (14)].t_bandwidth_list);
				} else
					(*SDP_parse_parsed_message).bandwidth() = OMIT_VALUE;

				(*SDP_parse_parsed_message).times() = *(yyvsp[(10) - (14)].t_sdp_time_list);
					delete (yyvsp[(10) - (14)].t_sdp_time_list);

				if ((yyvsp[(11) - (14)].t_sdp_timezone_list) != NULL) {
					(*SDP_parse_parsed_message).timezone__adjustments() = *(yyvsp[(11) - (14)].t_sdp_timezone_list);
					delete (yyvsp[(11) - (14)].t_sdp_timezone_list);
				} else
					(*SDP_parse_parsed_message).timezone__adjustments() = OMIT_VALUE;

				
				if ((yyvsp[(12) - (14)].t_sdp_key) != NULL) {
					(*SDP_parse_parsed_message).key () = *(yyvsp[(12) - (14)].t_sdp_key);
					delete (yyvsp[(12) - (14)].t_sdp_key);
				} else {
					(*SDP_parse_parsed_message).key() = OMIT_VALUE;
				}
				
				if ((yyvsp[(13) - (14)].t_sdp_attribute_list) != NULL) {
					(*SDP_parse_parsed_message).attributes() = *(yyvsp[(13) - (14)].t_sdp_attribute_list);
					delete (yyvsp[(13) - (14)].t_sdp_attribute_list);
				} else {
					(*SDP_parse_parsed_message).attributes() = OMIT_VALUE;
				}
				if ((yyvsp[(14) - (14)].t_sdp_media_desc_list) != NULL) {
					(*SDP_parse_parsed_message).media__list() = *(yyvsp[(14) - (14)].t_sdp_media_desc_list);
					delete (yyvsp[(14) - (14)].t_sdp_media_desc_list);
				} else 
					(*SDP_parse_parsed_message).media__list() = OMIT_VALUE;
				
				YYACCEPT;
			  }
    break;

  case 3:

/* Line 1806 of yacc.c  */
#line 258 "SDP_parser.y"
    {(yyval.t_sdp_media_desc_list) = NULL;}
    break;

  case 4:

/* Line 1806 of yacc.c  */
#line 259 "SDP_parser.y"
    {
						if ((yyvsp[(1) - (2)].t_sdp_media_desc_list) != NULL) {
							int media_num = (*(yyvsp[(1) - (2)].t_sdp_media_desc_list)).size_of();
							(*(yyvsp[(1) - (2)].t_sdp_media_desc_list))[media_num] = *(yyvsp[(2) - (2)].t_sdp_media_desc);
							delete (yyvsp[(2) - (2)].t_sdp_media_desc);
						} else {
							(yyval.t_sdp_media_desc_list) = new SDP__media__desc__list();
							(*(yyval.t_sdp_media_desc_list))[0] = *(yyvsp[(2) - (2)].t_sdp_media_desc);
							delete (yyvsp[(2) - (2)].t_sdp_media_desc);
						}
					}
    break;

  case 5:

/* Line 1806 of yacc.c  */
#line 277 "SDP_parser.y"
    {
						(yyval.t_sdp_media_desc) = new SDP__media__desc();
						(*(yyval.t_sdp_media_desc)).media__field() = *(yyvsp[(1) - (6)].t_sdp_media_field);
						delete (yyvsp[(1) - (6)].t_sdp_media_field);
						if ((yyvsp[(2) - (6)].t_charstring) != NULL) {
							(*(yyval.t_sdp_media_desc)).information() = *(yyvsp[(2) - (6)].t_charstring);
							delete (yyvsp[(2) - (6)].t_charstring);
						} else
							(*(yyval.t_sdp_media_desc)).information() = OMIT_VALUE;
						if ((yyvsp[(3) - (6)].t_sdp_connection_list) != NULL) {
							(*(yyval.t_sdp_media_desc)).connections() = *(yyvsp[(3) - (6)].t_sdp_connection_list);
							delete (yyvsp[(3) - (6)].t_sdp_connection_list);
						} else 
							(*(yyval.t_sdp_media_desc)).connections() = OMIT_VALUE;
						if ((yyvsp[(4) - (6)].t_bandwidth_list) != NULL) {
							(*(yyval.t_sdp_media_desc)).bandwidth() = *(yyvsp[(4) - (6)].t_bandwidth_list);
							delete (yyvsp[(4) - (6)].t_bandwidth_list);
						} else
							(*(yyval.t_sdp_media_desc)).bandwidth() = OMIT_VALUE;
						if ((yyvsp[(5) - (6)].t_sdp_key) != NULL) {
							(*(yyval.t_sdp_media_desc)).key() = *(yyvsp[(5) - (6)].t_sdp_key);
							delete (yyvsp[(5) - (6)].t_sdp_key);
						} else
							(*(yyval.t_sdp_media_desc)).key() = OMIT_VALUE;
						if ((yyvsp[(6) - (6)].t_sdp_attribute_list) != NULL) {
							(*(yyval.t_sdp_media_desc)).attributes() = *(yyvsp[(6) - (6)].t_sdp_attribute_list);
							delete (yyvsp[(6) - (6)].t_sdp_attribute_list);
						} else
							(*(yyval.t_sdp_media_desc)).attributes()=  OMIT_VALUE;
					}
    break;

  case 6:

/* Line 1806 of yacc.c  */
#line 309 "SDP_parser.y"
    {
						(yyval.t_sdp_media_field) = new SDP__media__field();
						(*(yyval.t_sdp_media_field)).media() = *(yyvsp[(2) - (11)].t_charstring);
						delete (yyvsp[(2) - (11)].t_charstring);
						(*(yyval.t_sdp_media_field)).ports().port__number() = str2int(*(yyvsp[(4) - (11)].t_charstring));
						delete (yyvsp[(4) - (11)].t_charstring);
						(*(yyval.t_sdp_media_field)).ports().num__of__ports() = str2int(*(yyvsp[(6) - (11)].t_charstring));
						delete (yyvsp[(6) - (11)].t_charstring);
						(*(yyval.t_sdp_media_field)).transport() = *(yyvsp[(8) - (11)].t_charstring);
						delete (yyvsp[(8) - (11)].t_charstring);
						(*(yyval.t_sdp_media_field)).fmts() = *(yyvsp[(10) - (11)].t_sdp_fmt_list);
						delete (yyvsp[(10) - (11)].t_sdp_fmt_list);
					}
    break;

  case 7:

/* Line 1806 of yacc.c  */
#line 323 "SDP_parser.y"
    {
						(yyval.t_sdp_media_field) = new SDP__media__field();
						(*(yyval.t_sdp_media_field)).media() = *(yyvsp[(2) - (9)].t_charstring);
						delete (yyvsp[(2) - (9)].t_charstring);
						(*(yyval.t_sdp_media_field)).ports().port__number() = str2int(*(yyvsp[(4) - (9)].t_charstring));
						delete (yyvsp[(4) - (9)].t_charstring);
						(*(yyval.t_sdp_media_field)).ports().num__of__ports() = OMIT_VALUE;
						(*(yyval.t_sdp_media_field)).transport() = *(yyvsp[(6) - (9)].t_charstring);
						delete (yyvsp[(6) - (9)].t_charstring);
						(*(yyval.t_sdp_media_field)).fmts() = *(yyvsp[(8) - (9)].t_sdp_fmt_list);
						delete (yyvsp[(8) - (9)].t_sdp_fmt_list);
					}
    break;

  case 8:

/* Line 1806 of yacc.c  */
#line 336 "SDP_parser.y"
    { (yyval.t_charstring)=(yyvsp[(1) - (1)].t_charstring);}
    break;

  case 9:

/* Line 1806 of yacc.c  */
#line 337 "SDP_parser.y"
    {
         const CHARSTRING& str = *(yyvsp[(1) - (3)].t_charstring)+CHARSTRING(1,"/")+*(yyvsp[(3) - (3)].t_charstring);
         delete (yyvsp[(1) - (3)].t_charstring);
         delete (yyvsp[(3) - (3)].t_charstring);
         (yyval.t_charstring) = new CHARSTRING(str);
         
       }
    break;

  case 10:

/* Line 1806 of yacc.c  */
#line 345 "SDP_parser.y"
    {
				(yyval.t_sdp_fmt_list) = new SDP__fmt__list();
				(*(yyval.t_sdp_fmt_list))[0] = *(yyvsp[(1) - (1)].t_charstring);
				delete (yyvsp[(1) - (1)].t_charstring);
			}
    break;

  case 11:

/* Line 1806 of yacc.c  */
#line 350 "SDP_parser.y"
    {
				int fmt_num = (*(yyvsp[(1) - (3)].t_sdp_fmt_list)).size_of();
				(*(yyvsp[(1) - (3)].t_sdp_fmt_list))[fmt_num] = *(yyvsp[(3) - (3)].t_charstring);
				delete (yyvsp[(3) - (3)].t_charstring);
				(yyval.t_sdp_fmt_list) = (yyvsp[(1) - (3)].t_sdp_fmt_list);
			}
    break;

  case 12:

/* Line 1806 of yacc.c  */
#line 357 "SDP_parser.y"
    {
						(yyval.t_sdp_attribute_list) = NULL;
					}
    break;

  case 13:

/* Line 1806 of yacc.c  */
#line 360 "SDP_parser.y"
    {
						if ((yyvsp[(1) - (2)].t_sdp_attribute_list) != NULL) {
							int att_num = (*(yyvsp[(1) - (2)].t_sdp_attribute_list)).size_of();
							(*(yyvsp[(1) - (2)].t_sdp_attribute_list))[att_num] = *(yyvsp[(2) - (2)].t_sdp_attribute);
							delete (yyvsp[(2) - (2)].t_sdp_attribute);
						} else {
							(yyval.t_sdp_attribute_list) = new SDP__attribute__list();
							(*(yyval.t_sdp_attribute_list))[0] = *(yyvsp[(2) - (2)].t_sdp_attribute);
							delete (yyvsp[(2) - (2)].t_sdp_attribute);
						}
					}
    break;

  case 14:

/* Line 1806 of yacc.c  */
#line 373 "SDP_parser.y"
    {
						(yyval.t_sdp_attribute) = (yyvsp[(2) - (3)].t_sdp_attribute);
					}
    break;

  case 15:

/* Line 1806 of yacc.c  */
#line 377 "SDP_parser.y"
    {
					(yyval.t_sdp_attribute) = new SDP__attribute();
					(*(yyval.t_sdp_attribute)).cat().attr__value() = *(yyvsp[(2) - (2)].t_charstring);
					delete (yyvsp[(2) - (2)].t_charstring);
				}
    break;

  case 16:

/* Line 1806 of yacc.c  */
#line 382 "SDP_parser.y"
    {
					(yyval.t_sdp_attribute) = new SDP__attribute();
					(*(yyval.t_sdp_attribute)).keywds().attr__value() = *(yyvsp[(2) - (2)].t_charstring);
					delete (yyvsp[(2) - (2)].t_charstring);
				}
    break;

  case 17:

/* Line 1806 of yacc.c  */
#line 387 "SDP_parser.y"
    {
					(yyval.t_sdp_attribute) = new SDP__attribute();
					(*(yyval.t_sdp_attribute)).tool().attr__value() = *(yyvsp[(2) - (2)].t_charstring);
					delete (yyvsp[(2) - (2)].t_charstring);
				}
    break;

  case 18:

/* Line 1806 of yacc.c  */
#line 392 "SDP_parser.y"
    {
					(yyval.t_sdp_attribute) = new SDP__attribute();
					(*(yyval.t_sdp_attribute)).ptime().attr__value() = *(yyvsp[(2) - (2)].t_charstring);
					delete (yyvsp[(2) - (2)].t_charstring);
				}
    break;

  case 19:

/* Line 1806 of yacc.c  */
#line 397 "SDP_parser.y"
    {
					(yyval.t_sdp_attribute) = new SDP__attribute();
					(*(yyval.t_sdp_attribute)).recvonly() = SDP__attribute__recvonly(NULL_VALUE);
				}
    break;

  case 20:

/* Line 1806 of yacc.c  */
#line 401 "SDP_parser.y"
    {
					(yyval.t_sdp_attribute) = new SDP__attribute();
					(*(yyval.t_sdp_attribute)).sendrecv() = SDP__attribute__sendrecv(NULL_VALUE);
				}
    break;

  case 21:

/* Line 1806 of yacc.c  */
#line 405 "SDP_parser.y"
    {
					(yyval.t_sdp_attribute) = new SDP__attribute();
					(*(yyval.t_sdp_attribute)).sendonly() = SDP__attribute__sendonly(NULL_VALUE);
				}
    break;

  case 22:

/* Line 1806 of yacc.c  */
#line 409 "SDP_parser.y"
    {
					(yyval.t_sdp_attribute) = new SDP__attribute();
					(*(yyval.t_sdp_attribute)).orient().attr__value() = *(yyvsp[(2) - (2)].t_charstring);
					delete (yyvsp[(2) - (2)].t_charstring);
				}
    break;

  case 23:

/* Line 1806 of yacc.c  */
#line 414 "SDP_parser.y"
    {
					(yyval.t_sdp_attribute) = new SDP__attribute();
					(*(yyval.t_sdp_attribute)).sdp__type().attr__value() = *(yyvsp[(2) - (2)].t_charstring);
					delete (yyvsp[(2) - (2)].t_charstring);
				}
    break;

  case 24:

/* Line 1806 of yacc.c  */
#line 419 "SDP_parser.y"
    {
					(yyval.t_sdp_attribute) = new SDP__attribute();
					(*(yyval.t_sdp_attribute)).charset().attr__value() = *(yyvsp[(2) - (2)].t_charstring);
					delete (yyvsp[(2) - (2)].t_charstring);
				}
    break;

  case 25:

/* Line 1806 of yacc.c  */
#line 424 "SDP_parser.y"
    {
					(yyval.t_sdp_attribute) = new SDP__attribute();
					(*(yyval.t_sdp_attribute)).sdplang().attr__value() = *(yyvsp[(2) - (2)].t_charstring);
					delete (yyvsp[(2) - (2)].t_charstring);
				}
    break;

  case 26:

/* Line 1806 of yacc.c  */
#line 429 "SDP_parser.y"
    {
					(yyval.t_sdp_attribute) = new SDP__attribute();
					(*(yyval.t_sdp_attribute)).lang().attr__value() = *(yyvsp[(2) - (2)].t_charstring);
					delete (yyvsp[(2) - (2)].t_charstring);
				}
    break;

  case 27:

/* Line 1806 of yacc.c  */
#line 434 "SDP_parser.y"
    {
					(yyval.t_sdp_attribute) = new SDP__attribute();
					(*(yyval.t_sdp_attribute)).framerate().attr__value() = *(yyvsp[(2) - (2)].t_charstring);
					delete (yyvsp[(2) - (2)].t_charstring);
				}
    break;

  case 28:

/* Line 1806 of yacc.c  */
#line 439 "SDP_parser.y"
    {
					(yyval.t_sdp_attribute) = new SDP__attribute();
					(*(yyval.t_sdp_attribute)).quality().attr__value() = *(yyvsp[(2) - (2)].t_charstring);
					delete (yyvsp[(2) - (2)].t_charstring);
				}
    break;

  case 29:

/* Line 1806 of yacc.c  */
#line 444 "SDP_parser.y"
    {
					(yyval.t_sdp_attribute) = new SDP__attribute();
					(*(yyval.t_sdp_attribute)).fmtp().attr__value() = *(yyvsp[(2) - (2)].t_charstring);
					delete (yyvsp[(2) - (2)].t_charstring);
				}
    break;

  case 30:

/* Line 1806 of yacc.c  */
#line 449 "SDP_parser.y"
    {
					(yyval.t_sdp_attribute) = new SDP__attribute();
					(*(yyval.t_sdp_attribute)).curr().attr__value() = *(yyvsp[(2) - (2)].t_charstring);
					delete (yyvsp[(2) - (2)].t_charstring);
				}
    break;

  case 31:

/* Line 1806 of yacc.c  */
#line 454 "SDP_parser.y"
    {
					(yyval.t_sdp_attribute) = new SDP__attribute();
					(*(yyval.t_sdp_attribute)).des().attr__value() = *(yyvsp[(2) - (2)].t_charstring);
					delete (yyvsp[(2) - (2)].t_charstring);
				}
    break;

  case 32:

/* Line 1806 of yacc.c  */
#line 459 "SDP_parser.y"
    {
					(yyval.t_sdp_attribute) = new SDP__attribute();
					(*(yyval.t_sdp_attribute)).conf().attr__value() = *(yyvsp[(2) - (2)].t_charstring);
					delete (yyvsp[(2) - (2)].t_charstring);
				}
    break;

  case 33:

/* Line 1806 of yacc.c  */
#line 464 "SDP_parser.y"
    {
					(yyval.t_sdp_attribute) = new SDP__attribute();
					(*(yyval.t_sdp_attribute)).rtpmap().attr__value() = *(yyvsp[(2) - (2)].t_charstring);
					delete (yyvsp[(2) - (2)].t_charstring);
				}
    break;

  case 34:

/* Line 1806 of yacc.c  */
#line 469 "SDP_parser.y"
    {
					(yyval.t_sdp_attribute) = new SDP__attribute();
					(*(yyval.t_sdp_attribute)).rtcp().attr__value() = *(yyvsp[(2) - (2)].t_charstring);
					delete (yyvsp[(2) - (2)].t_charstring);
				}
    break;

  case 35:

/* Line 1806 of yacc.c  */
#line 474 "SDP_parser.y"
    {
					(yyval.t_sdp_attribute) = new SDP__attribute();
					(*(yyval.t_sdp_attribute)).max__size().max__size() = str2int(*(yyvsp[(2) - (2)].t_charstring));
					delete (yyvsp[(2) - (2)].t_charstring);
				}
    break;

  case 36:

/* Line 1806 of yacc.c  */
#line 479 "SDP_parser.y"
    {
					(yyval.t_sdp_attribute) = new SDP__attribute();
					(*(yyval.t_sdp_attribute)).path().path__list() = *(yyvsp[(2) - (2)].t_url_list);
					delete (yyvsp[(2) - (2)].t_url_list);
				}
    break;

  case 37:

/* Line 1806 of yacc.c  */
#line 484 "SDP_parser.y"
    {
					(yyval.t_sdp_attribute) = new SDP__attribute();
					(*(yyval.t_sdp_attribute)).accept__types().attr__value() = *(yyvsp[(2) - (2)].t_type_list);
					delete (yyvsp[(2) - (2)].t_type_list);
				}
    break;

  case 38:

/* Line 1806 of yacc.c  */
#line 489 "SDP_parser.y"
    {
					(yyval.t_sdp_attribute) = new SDP__attribute();
					(*(yyval.t_sdp_attribute)).accept__wrapped__types().attr__value() = *(yyvsp[(2) - (2)].t_type_list);
					delete (yyvsp[(2) - (2)].t_type_list);
				}
    break;

  case 39:

/* Line 1806 of yacc.c  */
#line 494 "SDP_parser.y"
    {
					(yyval.t_sdp_attribute) = new SDP__attribute();
					(*(yyval.t_sdp_attribute)).maxprate().prate__integer__part() = str2int(*(yyvsp[(2) - (2)].t_charstring));
					(*(yyval.t_sdp_attribute)).maxprate().prate__fraction__part() = OMIT_VALUE;
					delete (yyvsp[(2) - (2)].t_charstring);
				}
    break;

  case 40:

/* Line 1806 of yacc.c  */
#line 500 "SDP_parser.y"
    {
					(yyval.t_sdp_attribute) = new SDP__attribute();
					(*(yyval.t_sdp_attribute)).maxprate().prate__integer__part() = str2int(*(yyvsp[(2) - (4)].t_charstring));
					(*(yyval.t_sdp_attribute)).maxprate().prate__fraction__part() = str2int(*(yyvsp[(4) - (4)].t_charstring));
					delete (yyvsp[(2) - (4)].t_charstring);
					delete (yyvsp[(4) - (4)].t_charstring);
				}
    break;

  case 41:

/* Line 1806 of yacc.c  */
#line 507 "SDP_parser.y"
    {
					(yyval.t_sdp_attribute) = new SDP__attribute();
					(*(yyval.t_sdp_attribute)).mid().id__tag() = *(yyvsp[(2) - (2)].t_charstring);
					delete (yyvsp[(2) - (2)].t_charstring);
				}
    break;

  case 42:

/* Line 1806 of yacc.c  */
#line 512 "SDP_parser.y"
    {
					(yyval.t_sdp_attribute) = new SDP__attribute();
					(*(yyval.t_sdp_attribute)).group__attr().semantics() = *(yyvsp[(2) - (2)].t_charstring);
					(*(yyval.t_sdp_attribute)).group__attr().id__tag()=OMIT_VALUE;
					delete (yyvsp[(2) - (2)].t_charstring);
				}
    break;

  case 43:

/* Line 1806 of yacc.c  */
#line 518 "SDP_parser.y"
    {
					(yyval.t_sdp_attribute) = new SDP__attribute();
					(*(yyval.t_sdp_attribute)).group__attr().semantics() = *(yyvsp[(2) - (4)].t_charstring);
					(*(yyval.t_sdp_attribute)).group__attr().id__tag()=*(yyvsp[(4) - (4)].t_id_list);
					delete (yyvsp[(2) - (4)].t_charstring);
					delete (yyvsp[(4) - (4)].t_id_list);
				}
    break;

  case 44:

/* Line 1806 of yacc.c  */
#line 525 "SDP_parser.y"
    {
					(yyval.t_sdp_attribute) = new SDP__attribute();
					(*(yyval.t_sdp_attribute)).file__selector().attr__value() = *(yyvsp[(2) - (2)].t_charstring);
					delete (yyvsp[(2) - (2)].t_charstring);
				}
    break;

  case 45:

/* Line 1806 of yacc.c  */
#line 530 "SDP_parser.y"
    {
					(yyval.t_sdp_attribute) = new SDP__attribute();
					(*(yyval.t_sdp_attribute)).file__transfer__id().attr__value() = *(yyvsp[(2) - (2)].t_charstring);
					delete (yyvsp[(2) - (2)].t_charstring);
				}
    break;

  case 46:

/* Line 1806 of yacc.c  */
#line 535 "SDP_parser.y"
    {
					(yyval.t_sdp_attribute) = new SDP__attribute();
					(*(yyval.t_sdp_attribute)).inactive() = SDP__attribute__inactive(NULL_VALUE);
				}
    break;

  case 47:

/* Line 1806 of yacc.c  */
#line 539 "SDP_parser.y"
    {
					(yyval.t_sdp_attribute) = new SDP__attribute();
					(*(yyval.t_sdp_attribute)).rtcp__mux() = SDP__attribute__rtcp__mux(NULL_VALUE);
				}
    break;

  case 48:

/* Line 1806 of yacc.c  */
#line 543 "SDP_parser.y"
    {
					(yyval.t_sdp_attribute) = new SDP__attribute();
					(*(yyval.t_sdp_attribute)).setup().attr__value() = *(yyvsp[(2) - (2)].t_charstring);
					delete (yyvsp[(2) - (2)].t_charstring);
				}
    break;

  case 49:

/* Line 1806 of yacc.c  */
#line 548 "SDP_parser.y"
    {
					(yyval.t_sdp_attribute) = new SDP__attribute();
					(*(yyval.t_sdp_attribute)).connection().attr__value() = *(yyvsp[(2) - (2)].t_charstring);
					delete (yyvsp[(2) - (2)].t_charstring);
				}
    break;

  case 50:

/* Line 1806 of yacc.c  */
#line 553 "SDP_parser.y"
    {
					(yyval.t_sdp_attribute) = new SDP__attribute();
					(*(yyval.t_sdp_attribute)).crypto().attr__value() = *(yyvsp[(2) - (2)].t_charstring);
					delete (yyvsp[(2) - (2)].t_charstring);
				}
    break;

  case 51:

/* Line 1806 of yacc.c  */
#line 558 "SDP_parser.y"
    {
					(yyval.t_sdp_attribute) = new SDP__attribute();
					(*(yyval.t_sdp_attribute)).content().attr__value() = *(yyvsp[(2) - (2)].t_charstring);
					delete (yyvsp[(2) - (2)].t_charstring);
				}
    break;

  case 52:

/* Line 1806 of yacc.c  */
#line 563 "SDP_parser.y"
    {
					(yyval.t_sdp_attribute) = new SDP__attribute();
					(*(yyval.t_sdp_attribute)).attr__label().attr__value() = *(yyvsp[(2) - (2)].t_charstring);
					delete (yyvsp[(2) - (2)].t_charstring);
				}
    break;

  case 53:

/* Line 1806 of yacc.c  */
#line 568 "SDP_parser.y"
    {
					(yyval.t_sdp_attribute) = new SDP__attribute();
					(*(yyval.t_sdp_attribute)).floorctrl().attr__value() = *(yyvsp[(2) - (2)].t_charstring);
					delete (yyvsp[(2) - (2)].t_charstring);
				}
    break;

  case 54:

/* Line 1806 of yacc.c  */
#line 573 "SDP_parser.y"
    {
					(yyval.t_sdp_attribute) = new SDP__attribute();
					(*(yyval.t_sdp_attribute)).confid().attr__value() = *(yyvsp[(2) - (2)].t_charstring);
					delete (yyvsp[(2) - (2)].t_charstring);
				}
    break;

  case 55:

/* Line 1806 of yacc.c  */
#line 578 "SDP_parser.y"
    {
					(yyval.t_sdp_attribute) = new SDP__attribute();
					(*(yyval.t_sdp_attribute)).userid().attr__value() = *(yyvsp[(2) - (2)].t_charstring);
					delete (yyvsp[(2) - (2)].t_charstring);
				}
    break;

  case 56:

/* Line 1806 of yacc.c  */
#line 583 "SDP_parser.y"
    {
					(yyval.t_sdp_attribute) = new SDP__attribute();
					(*(yyval.t_sdp_attribute)).floorid().attr__value() = *(yyvsp[(2) - (2)].t_charstring);
					delete (yyvsp[(2) - (2)].t_charstring);
				}
    break;

  case 57:

/* Line 1806 of yacc.c  */
#line 588 "SDP_parser.y"
    {
					(yyval.t_sdp_attribute) = new SDP__attribute();
					(*(yyval.t_sdp_attribute)).fingerprint().attr__value() = *(yyvsp[(2) - (2)].t_charstring);
					delete (yyvsp[(2) - (2)].t_charstring);
				}
    break;

  case 58:

/* Line 1806 of yacc.c  */
#line 593 "SDP_parser.y"
    {
				        (yyval.t_sdp_attribute) = new SDP__attribute();
				        (*(yyval.t_sdp_attribute)).ice__ufrag().attr__value() = *(yyvsp[(2) - (2)].t_charstring);
					delete (yyvsp[(2) - (2)].t_charstring);
				}
    break;

  case 59:

/* Line 1806 of yacc.c  */
#line 598 "SDP_parser.y"
    {
				        (yyval.t_sdp_attribute) = new SDP__attribute();
				        (*(yyval.t_sdp_attribute)).ice__pwd().attr__value() = *(yyvsp[(2) - (2)].t_charstring);
					delete (yyvsp[(2) - (2)].t_charstring);
				}
    break;

  case 60:

/* Line 1806 of yacc.c  */
#line 605 "SDP_parser.y"
    {
				        (yyval.t_sdp_attribute) = new SDP__attribute();
				        (*(yyval.t_sdp_attribute)).candidate().foundation() = *(yyvsp[(2) - (19)].t_charstring); 
				        delete (yyvsp[(2) - (19)].t_charstring);
			                (*(yyval.t_sdp_attribute)).candidate().component__id() = str2int(*(yyvsp[(4) - (19)].t_charstring));
			                delete (yyvsp[(4) - (19)].t_charstring);
			                (*(yyval.t_sdp_attribute)).candidate().transport() = *(yyvsp[(6) - (19)].t_charstring); 
			                delete (yyvsp[(6) - (19)].t_charstring);
			                (*(yyval.t_sdp_attribute)).candidate().priority() = str2int(*(yyvsp[(8) - (19)].t_charstring));
				        delete (yyvsp[(8) - (19)].t_charstring);
					(*(yyval.t_sdp_attribute)).candidate().connection__address() = *(yyvsp[(10) - (19)].t_charstring);  
				        delete (yyvsp[(10) - (19)].t_charstring);
					(*(yyval.t_sdp_attribute)).candidate().connection__port() = str2int(*(yyvsp[(12) - (19)].t_charstring)); 
					delete (yyvsp[(12) - (19)].t_charstring);					  
					delete (yyvsp[(14) - (19)].t_charstring);
					(*(yyval.t_sdp_attribute)).candidate().candidate__type() = *(yyvsp[(16) - (19)].t_charstring);  
					delete (yyvsp[(16) - (19)].t_charstring);					
					if ((yyvsp[(17) - (19)].t_charstring) != NULL) {
							(*(yyval.t_sdp_attribute)).candidate().rel__address() = *(yyvsp[(17) - (19)].t_charstring);
							delete (yyvsp[(17) - (19)].t_charstring);
						} else
							(*(yyval.t_sdp_attribute)).candidate().rel__address() = OMIT_VALUE;
                                        if ((yyvsp[(18) - (19)].t_charstring) != NULL) {
							(*(yyval.t_sdp_attribute)).candidate().rel__port() = *(yyvsp[(18) - (19)].t_charstring);
							delete (yyvsp[(18) - (19)].t_charstring);
						} else
							(*(yyval.t_sdp_attribute)).candidate().rel__port() = OMIT_VALUE;					
				
				        if ((yyvsp[(19) - (19)].t_extension_list) != NULL) {
							(*(yyval.t_sdp_attribute)).candidate().extensions() = *(yyvsp[(19) - (19)].t_extension_list);
							delete (yyvsp[(19) - (19)].t_extension_list);
						} else
							(*(yyval.t_sdp_attribute)).candidate().extensions() = OMIT_VALUE;	
				}
    break;

  case 61:

/* Line 1806 of yacc.c  */
#line 639 "SDP_parser.y"
    {
					(yyval.t_sdp_attribute) = new SDP__attribute();
					(*(yyval.t_sdp_attribute)).ice__lite() = SDP__attribute__ice__lite(NULL_VALUE);
				}
    break;

  case 62:

/* Line 1806 of yacc.c  */
#line 643 "SDP_parser.y"
    {
					(yyval.t_sdp_attribute) = new SDP__attribute();
					(*(yyval.t_sdp_attribute)).ice__mismatch() = SDP__attribute__ice__mismatch(NULL_VALUE);
				}
    break;

  case 63:

/* Line 1806 of yacc.c  */
#line 648 "SDP_parser.y"
    {   
				        (yyval.t_sdp_attribute) = new SDP__attribute();
                                        (*(yyval.t_sdp_attribute)).remote__candidate().attr__value() = *(yyvsp[(2) - (2)].t_candidate_list);
                                        delete (yyvsp[(2) - (2)].t_candidate_list);
                                }
    break;

  case 64:

/* Line 1806 of yacc.c  */
#line 653 "SDP_parser.y"
    {
					(yyval.t_sdp_attribute) = new SDP__attribute();
					(*(yyval.t_sdp_attribute)).ice__options().attr__value() = *(yyvsp[(2) - (2)].t_sdp_ice_option_list);
					delete (yyvsp[(2) - (2)].t_sdp_ice_option_list);
				}
    break;

  case 65:

/* Line 1806 of yacc.c  */
#line 658 "SDP_parser.y"
    {
				        (yyval.t_sdp_attribute) = new SDP__attribute();
				        (*(yyval.t_sdp_attribute)).rtcp__fb().attr__value() = *(yyvsp[(2) - (2)].t_charstring);
					delete (yyvsp[(2) - (2)].t_charstring);
				}
    break;

  case 66:

/* Line 1806 of yacc.c  */
#line 663 "SDP_parser.y"
    {				
				        (yyval.t_sdp_attribute) = new SDP__attribute();				
				        (*(yyval.t_sdp_attribute)).maxptime().attr__value() = *(yyvsp[(2) - (2)].t_charstring);				
					delete (yyvsp[(2) - (2)].t_charstring);
				}
    break;

  case 67:

/* Line 1806 of yacc.c  */
#line 668 "SDP_parser.y"
    {				
				        (yyval.t_sdp_attribute) = new SDP__attribute();				
				        (*(yyval.t_sdp_attribute)).t38__version().attr__value() = str2int(*(yyvsp[(2) - (2)].t_charstring));				
					delete (yyvsp[(2) - (2)].t_charstring);
				}
    break;

  case 68:

/* Line 1806 of yacc.c  */
#line 673 "SDP_parser.y"
    {				
				        (yyval.t_sdp_attribute) = new SDP__attribute();				
				        (*(yyval.t_sdp_attribute)).t38__bit__rate().attr__value() = str2int(*(yyvsp[(2) - (2)].t_charstring));				
					delete (yyvsp[(2) - (2)].t_charstring);
				}
    break;

  case 69:

/* Line 1806 of yacc.c  */
#line 678 "SDP_parser.y"
    {				
				        (yyval.t_sdp_attribute) = new SDP__attribute();				
				        (*(yyval.t_sdp_attribute)).t38__fill__bit__removal().attr__value() = str2int(*(yyvsp[(2) - (2)].t_charstring));				
					delete (yyvsp[(2) - (2)].t_charstring);
				}
    break;

  case 70:

/* Line 1806 of yacc.c  */
#line 683 "SDP_parser.y"
    {				
				        (yyval.t_sdp_attribute) = new SDP__attribute();				
				        (*(yyval.t_sdp_attribute)).t38__transcoding__mmr().attr__value() = str2int(*(yyvsp[(2) - (2)].t_charstring));				
					delete (yyvsp[(2) - (2)].t_charstring);
				}
    break;

  case 71:

/* Line 1806 of yacc.c  */
#line 688 "SDP_parser.y"
    {				
				        (yyval.t_sdp_attribute) = new SDP__attribute();				
				        (*(yyval.t_sdp_attribute)).t38__transcoding__jbig().attr__value() = str2int(*(yyvsp[(2) - (2)].t_charstring));				
					delete (yyvsp[(2) - (2)].t_charstring);
				}
    break;

  case 72:

/* Line 1806 of yacc.c  */
#line 693 "SDP_parser.y"
    {				
				        (yyval.t_sdp_attribute) = new SDP__attribute();				
				        (*(yyval.t_sdp_attribute)).t38__rate__management().attr__value() = *(yyvsp[(2) - (2)].t_charstring);				
					delete (yyvsp[(2) - (2)].t_charstring);
				}
    break;

  case 73:

/* Line 1806 of yacc.c  */
#line 698 "SDP_parser.y"
    {				
				        (yyval.t_sdp_attribute) = new SDP__attribute();				
				        (*(yyval.t_sdp_attribute)).t38__max__buffer().attr__value() = str2int(*(yyvsp[(2) - (2)].t_charstring));				
					delete (yyvsp[(2) - (2)].t_charstring);
				}
    break;

  case 74:

/* Line 1806 of yacc.c  */
#line 703 "SDP_parser.y"
    {				
				        (yyval.t_sdp_attribute) = new SDP__attribute();				
				        (*(yyval.t_sdp_attribute)).t38__max__datagram().attr__value() = str2int(*(yyvsp[(2) - (2)].t_charstring));				
					delete (yyvsp[(2) - (2)].t_charstring);
				}
    break;

  case 75:

/* Line 1806 of yacc.c  */
#line 708 "SDP_parser.y"
    {				
				        (yyval.t_sdp_attribute) = new SDP__attribute();				
				        (*(yyval.t_sdp_attribute)).t38__max__ifp().attr__value() = str2int(*(yyvsp[(2) - (2)].t_charstring));				
					delete (yyvsp[(2) - (2)].t_charstring);
				}
    break;

  case 76:

/* Line 1806 of yacc.c  */
#line 713 "SDP_parser.y"
    {				
				        (yyval.t_sdp_attribute) = new SDP__attribute();				
				        (*(yyval.t_sdp_attribute)).t38__udp__ec().attr__value() = *(yyvsp[(2) - (2)].t_charstring);				
					delete (yyvsp[(2) - (2)].t_charstring);
				}
    break;

  case 77:

/* Line 1806 of yacc.c  */
#line 718 "SDP_parser.y"
    {				
				        (yyval.t_sdp_attribute) = new SDP__attribute();				
				        (*(yyval.t_sdp_attribute)).t38__udp__ec__depth().minred() = str2int(*(yyvsp[(2) - (2)].t_charstring));
                                        (*(yyval.t_sdp_attribute)).t38__udp__ec__depth().maxred() = OMIT_VALUE;				
					delete (yyvsp[(2) - (2)].t_charstring);
				}
    break;

  case 78:

/* Line 1806 of yacc.c  */
#line 724 "SDP_parser.y"
    {				
				        (yyval.t_sdp_attribute) = new SDP__attribute();				
				        (*(yyval.t_sdp_attribute)).t38__udp__ec__depth().minred() = str2int(*(yyvsp[(2) - (4)].t_charstring));
                                        (*(yyval.t_sdp_attribute)).t38__udp__ec__depth().maxred() = str2int(*(yyvsp[(4) - (4)].t_charstring));				
					delete (yyvsp[(2) - (4)].t_charstring);
                                        delete (yyvsp[(4) - (4)].t_charstring);
				}
    break;

  case 79:

/* Line 1806 of yacc.c  */
#line 731 "SDP_parser.y"
    {				
				        (yyval.t_sdp_attribute) = new SDP__attribute();				
				        (*(yyval.t_sdp_attribute)).t38__udp__fec__max__spam().attr__value() = str2int(*(yyvsp[(2) - (2)].t_charstring));				
					delete (yyvsp[(2) - (2)].t_charstring);
				}
    break;

  case 80:

/* Line 1806 of yacc.c  */
#line 736 "SDP_parser.y"
    {				
				        (yyval.t_sdp_attribute) = new SDP__attribute();				
                                        (*(yyval.t_sdp_attribute)).t38__vendor__info().t35country__code() = str2int(*(yyvsp[(2) - (6)].t_charstring));
                                        (*(yyval.t_sdp_attribute)).t38__vendor__info().t35extension() = str2int(*(yyvsp[(4) - (6)].t_charstring));
                                        (*(yyval.t_sdp_attribute)).t38__vendor__info().manufacturer__code() = str2int(*(yyvsp[(6) - (6)].t_charstring));				
                                        delete (yyvsp[(2) - (6)].t_charstring);
                                        delete (yyvsp[(4) - (6)].t_charstring);				
					delete (yyvsp[(6) - (6)].t_charstring);
				}
    break;

  case 81:

/* Line 1806 of yacc.c  */
#line 745 "SDP_parser.y"
    {				
				        (yyval.t_sdp_attribute) = new SDP__attribute();				
				        (*(yyval.t_sdp_attribute)).t38__modem__type().attr__value() = *(yyvsp[(2) - (2)].t_charstring);				
					delete (yyvsp[(2) - (2)].t_charstring);
				}
    break;

  case 82:

/* Line 1806 of yacc.c  */
#line 750 "SDP_parser.y"
    {
					(yyval.t_sdp_attribute) = new SDP__attribute();
					(*(yyval.t_sdp_attribute)).file__disposition().attr__value() = *(yyvsp[(2) - (2)].t_charstring);
					delete (yyvsp[(2) - (2)].t_charstring);
				}
    break;

  case 83:

/* Line 1806 of yacc.c  */
#line 755 "SDP_parser.y"
    {
					(yyval.t_sdp_attribute) = new SDP__attribute();
					(*(yyval.t_sdp_attribute)).file__date().attr__value() = *(yyvsp[(2) - (2)].t_charstring);
					delete (yyvsp[(2) - (2)].t_charstring);
				}
    break;

  case 84:

/* Line 1806 of yacc.c  */
#line 760 "SDP_parser.y"
    {
					(yyval.t_sdp_attribute) = new SDP__attribute();
					(*(yyval.t_sdp_attribute)).file__icon().attr__value() = *(yyvsp[(2) - (2)].t_charstring);
					delete (yyvsp[(2) - (2)].t_charstring);
				}
    break;

  case 85:

/* Line 1806 of yacc.c  */
#line 765 "SDP_parser.y"
    {
					(yyval.t_sdp_attribute) = new SDP__attribute();
					(*(yyval.t_sdp_attribute)).file__range().attr__value() = *(yyvsp[(2) - (2)].t_charstring);
					delete (yyvsp[(2) - (2)].t_charstring);
				}
    break;

  case 86:

/* Line 1806 of yacc.c  */
#line 770 "SDP_parser.y"
    {
					(yyval.t_sdp_attribute) = new SDP__attribute();
					(*(yyval.t_sdp_attribute)).sctpmap().attr__value() = *(yyvsp[(2) - (2)].t_charstring);
					delete (yyvsp[(2) - (2)].t_charstring);
				}
    break;

  case 87:

/* Line 1806 of yacc.c  */
#line 775 "SDP_parser.y"
    {
					(yyval.t_sdp_attribute) = new SDP__attribute();
					(*(yyval.t_sdp_attribute)).sctp__port().attr__value() = str2int(*(yyvsp[(2) - (2)].t_charstring));
					delete (yyvsp[(2) - (2)].t_charstring);
				}
    break;

  case 88:

/* Line 1806 of yacc.c  */
#line 780 "SDP_parser.y"
    {
					(yyval.t_sdp_attribute) = new SDP__attribute();
					(*(yyval.t_sdp_attribute)).max__message__size().attr__value() = str2int(*(yyvsp[(2) - (2)].t_charstring));
					delete (yyvsp[(2) - (2)].t_charstring);
				}
    break;

  case 89:

/* Line 1806 of yacc.c  */
#line 786 "SDP_parser.y"
    {
					(yyval.t_sdp_attribute) = new SDP__attribute();
					(*(yyval.t_sdp_attribute)).unknown().name() = *(yyvsp[(1) - (3)].t_charstring);
					delete (yyvsp[(1) - (3)].t_charstring);
					(*(yyval.t_sdp_attribute)).unknown().attr__value() = *(yyvsp[(3) - (3)].t_charstring);
					delete (yyvsp[(3) - (3)].t_charstring);
                                }
    break;

  case 90:

/* Line 1806 of yacc.c  */
#line 793 "SDP_parser.y"
    {
					(yyval.t_sdp_attribute) = new SDP__attribute();
					(*(yyval.t_sdp_attribute)).unknown().name() = *(yyvsp[(1) - (1)].t_charstring);
					delete (yyvsp[(1) - (1)].t_charstring);
					(*(yyval.t_sdp_attribute)).unknown().attr__value() = OMIT_VALUE;
				}
    break;

  case 91:

/* Line 1806 of yacc.c  */
#line 800 "SDP_parser.y"
    {
					(yyval.t_sdp_ice_option_list) = new SDP__ice__options__list();
					(*(yyval.t_sdp_ice_option_list))[0] = *(yyvsp[(1) - (1)].t_charstring);
					delete (yyvsp[(1) - (1)].t_charstring);
				}
    break;

  case 92:

/* Line 1806 of yacc.c  */
#line 805 "SDP_parser.y"
    {
					int ice_option_num = (*(yyvsp[(1) - (3)].t_sdp_ice_option_list)).size_of();
					(*(yyvsp[(1) - (3)].t_sdp_ice_option_list))[ice_option_num] = *(yyvsp[(3) - (3)].t_charstring);
					delete (yyvsp[(3) - (3)].t_charstring);
					(yyval.t_sdp_ice_option_list) = (yyvsp[(1) - (3)].t_sdp_ice_option_list);
				}
    break;

  case 93:

/* Line 1806 of yacc.c  */
#line 812 "SDP_parser.y"
    {
					(yyval.t_candidate_list) = new SDP__Remote__candidate__list();
					(*(yyval.t_candidate_list))[0] = *(yyvsp[(1) - (1)].t_candidate);
					delete (yyvsp[(1) - (1)].t_candidate);
				}
    break;

  case 94:

/* Line 1806 of yacc.c  */
#line 817 "SDP_parser.y"
    {
					int candidate_num = (*(yyvsp[(1) - (3)].t_candidate_list)).size_of();
					(*(yyvsp[(1) - (3)].t_candidate_list))[candidate_num] = *(yyvsp[(3) - (3)].t_candidate);
					delete (yyvsp[(3) - (3)].t_candidate);
					(yyval.t_candidate_list) = (yyvsp[(1) - (3)].t_candidate_list);
				}
    break;

  case 95:

/* Line 1806 of yacc.c  */
#line 824 "SDP_parser.y"
    {
						(yyval.t_candidate) = new SDP__Remote__candidate();
						(*(yyval.t_candidate)).component__Id() = *(yyvsp[(1) - (5)].t_charstring);
						delete (yyvsp[(1) - (5)].t_charstring);
						(*(yyval.t_candidate)).connection__address() = *(yyvsp[(3) - (5)].t_charstring);
						delete (yyvsp[(3) - (5)].t_charstring);
					        (*(yyval.t_candidate)).portValue() = str2int(*(yyvsp[(5) - (5)].t_charstring));
						delete (yyvsp[(5) - (5)].t_charstring);							
					}
    break;

  case 96:

/* Line 1806 of yacc.c  */
#line 834 "SDP_parser.y"
    {(yyval.t_charstring) = NULL;}
    break;

  case 97:

/* Line 1806 of yacc.c  */
#line 835 "SDP_parser.y"
    {
			      delete (yyvsp[(2) - (4)].t_charstring);
	                      (yyval.t_charstring) = (yyvsp[(4) - (4)].t_charstring);			     
			}
    break;

  case 98:

/* Line 1806 of yacc.c  */
#line 841 "SDP_parser.y"
    {(yyval.t_charstring) = NULL;}
    break;

  case 99:

/* Line 1806 of yacc.c  */
#line 842 "SDP_parser.y"
    {
			      delete (yyvsp[(2) - (4)].t_charstring);
	                      (yyval.t_charstring) = (yyvsp[(4) - (4)].t_charstring);
			}
    break;

  case 100:

/* Line 1806 of yacc.c  */
#line 847 "SDP_parser.y"
    {(yyval.t_extension_list) = NULL;}
    break;

  case 101:

/* Line 1806 of yacc.c  */
#line 848 "SDP_parser.y"
    {
			 if ((yyvsp[(1) - (2)].t_extension_list) != NULL) {
						int extension_num = (*(yyvsp[(1) - (2)].t_extension_list)).size_of();
						(*(yyvsp[(1) - (2)].t_extension_list))[extension_num] = *(yyvsp[(2) - (2)].t_extension);
						delete (yyvsp[(2) - (2)].t_extension);
						(yyval.t_extension_list) = (yyvsp[(1) - (2)].t_extension_list);
					} else {
						(yyval.t_extension_list) = new SDP__extension__list();
						(*(yyval.t_extension_list))[0] = *(yyvsp[(2) - (2)].t_extension);
						delete (yyvsp[(2) - (2)].t_extension);
					}
				}
    break;

  case 102:

/* Line 1806 of yacc.c  */
#line 861 "SDP_parser.y"
    {
						(yyval.t_extension) = new SDP__extension();
						(*(yyval.t_extension)).extension__attr__name() = *(yyvsp[(2) - (4)].t_charstring);
						delete (yyvsp[(2) - (4)].t_charstring);
						(*(yyval.t_extension)).extension__attr__value() = *(yyvsp[(4) - (4)].t_charstring);
						delete (yyvsp[(4) - (4)].t_charstring);
					}
    break;

  case 103:

/* Line 1806 of yacc.c  */
#line 870 "SDP_parser.y"
    {
					(yyval.t_url_list) = new SDP__url__list();
					(*(yyval.t_url_list))[0] = *(yyvsp[(1) - (1)].t_charstring);
					delete (yyvsp[(1) - (1)].t_charstring);
				}
    break;

  case 104:

/* Line 1806 of yacc.c  */
#line 875 "SDP_parser.y"
    {
					int url_num = (*(yyvsp[(1) - (3)].t_url_list)).size_of();
					(*(yyvsp[(1) - (3)].t_url_list))[url_num] = *(yyvsp[(3) - (3)].t_charstring);
					delete (yyvsp[(3) - (3)].t_charstring);
					(yyval.t_url_list) = (yyvsp[(1) - (3)].t_url_list);
				}
    break;

  case 105:

/* Line 1806 of yacc.c  */
#line 882 "SDP_parser.y"
    {
					(yyval.t_id_list) = new SDP__id__tag__list();
					(*(yyval.t_id_list))[0] = *(yyvsp[(1) - (1)].t_charstring);
					delete (yyvsp[(1) - (1)].t_charstring);
				}
    break;

  case 106:

/* Line 1806 of yacc.c  */
#line 887 "SDP_parser.y"
    {
					int id_num = (*(yyvsp[(1) - (3)].t_id_list)).size_of();
					(*(yyvsp[(1) - (3)].t_id_list))[id_num] = *(yyvsp[(3) - (3)].t_charstring);
					delete (yyvsp[(3) - (3)].t_charstring);
					(yyval.t_id_list) = (yyvsp[(1) - (3)].t_id_list);
				}
    break;

  case 107:

/* Line 1806 of yacc.c  */
#line 894 "SDP_parser.y"
    {
					(yyval.t_type_list) = new SDP__media__type__list();
					(*(yyval.t_type_list))[0] = *(yyvsp[(1) - (1)].t_charstring);
					delete (yyvsp[(1) - (1)].t_charstring);
				}
    break;

  case 108:

/* Line 1806 of yacc.c  */
#line 899 "SDP_parser.y"
    {
					int url_num = (*(yyvsp[(1) - (3)].t_type_list)).size_of();
					(*(yyvsp[(1) - (3)].t_type_list))[url_num] = *(yyvsp[(3) - (3)].t_charstring);
					delete (yyvsp[(3) - (3)].t_charstring);
					(yyval.t_type_list) = (yyvsp[(1) - (3)].t_type_list);
				}
    break;

  case 109:

/* Line 1806 of yacc.c  */
#line 907 "SDP_parser.y"
    {(yyval.t_sdp_key) = NULL;}
    break;

  case 110:

/* Line 1806 of yacc.c  */
#line 908 "SDP_parser.y"
    {
				(yyval.t_sdp_key) = (yyvsp[(2) - (3)].t_sdp_key);
				
			}
    break;

  case 111:

/* Line 1806 of yacc.c  */
#line 913 "SDP_parser.y"
    {
				(yyval.t_sdp_key) = new SDP__key();
				(*(yyval.t_sdp_key)).method() = *(yyvsp[(1) - (1)].t_charstring);
				delete (yyvsp[(1) - (1)].t_charstring);
				(*(yyval.t_sdp_key)).key() = OMIT_VALUE;
			}
    break;

  case 112:

/* Line 1806 of yacc.c  */
#line 919 "SDP_parser.y"
    {
				(yyval.t_sdp_key) = new SDP__key();
				(*(yyval.t_sdp_key)).method() = *(yyvsp[(1) - (3)].t_charstring);
				delete (yyvsp[(1) - (3)].t_charstring);
				(*(yyval.t_sdp_key)).key() = *(yyvsp[(3) - (3)].t_charstring);
				delete (yyvsp[(3) - (3)].t_charstring);
			}
    break;

  case 113:

/* Line 1806 of yacc.c  */
#line 929 "SDP_parser.y"
    {
					(yyval.t_sdp_time_list) = new SDP__time__list();
					(*(yyval.t_sdp_time_list))[0] = *(yyvsp[(1) - (1)].t_sdp_time);
					delete (yyvsp[(1) - (1)].t_sdp_time);
				}
    break;

  case 114:

/* Line 1806 of yacc.c  */
#line 934 "SDP_parser.y"
    {
					int time_num = (*(yyvsp[(1) - (2)].t_sdp_time_list)).size_of();
					(*(yyvsp[(1) - (2)].t_sdp_time_list))[time_num] = *(yyvsp[(2) - (2)].t_sdp_time);
					delete (yyvsp[(2) - (2)].t_sdp_time);
					(yyval.t_sdp_time_list) = (yyvsp[(1) - (2)].t_sdp_time_list);
				}
    break;

  case 115:

/* Line 1806 of yacc.c  */
#line 941 "SDP_parser.y"
    {
				(yyval.t_sdp_time) = new SDP__time;
				(*(yyval.t_sdp_time)).time__field() = *(yyvsp[(1) - (2)].t_sdp_time_field);
				delete (yyvsp[(1) - (2)].t_sdp_time_field);
				if ((yyvsp[(2) - (2)].t_sdp_repeat_list) != NULL) {
					(*(yyval.t_sdp_time)).time__repeat() = *(yyvsp[(2) - (2)].t_sdp_repeat_list);
					delete (yyvsp[(2) - (2)].t_sdp_repeat_list);
				} else
					(*(yyval.t_sdp_time)).time__repeat() = OMIT_VALUE;
			}
    break;

  case 116:

/* Line 1806 of yacc.c  */
#line 952 "SDP_parser.y"
    {(yyval.t_sdp_repeat_list) = NULL;}
    break;

  case 117:

/* Line 1806 of yacc.c  */
#line 953 "SDP_parser.y"
    {
					if ((yyvsp[(1) - (2)].t_sdp_repeat_list) != NULL) {
						int repeat_num = (*(yyvsp[(1) - (2)].t_sdp_repeat_list)).size_of();
						(*(yyvsp[(1) - (2)].t_sdp_repeat_list))[repeat_num] = *(yyvsp[(2) - (2)].t_sdp_repeat);
						delete (yyvsp[(2) - (2)].t_sdp_repeat);
						(yyval.t_sdp_repeat_list) = (yyvsp[(1) - (2)].t_sdp_repeat_list);
					} else {
						(yyval.t_sdp_repeat_list) = new SDP__repeat__list();
						(*(yyval.t_sdp_repeat_list))[0] = *(yyvsp[(2) - (2)].t_sdp_repeat);
						delete (yyvsp[(2) - (2)].t_sdp_repeat);
					}
				}
    break;

  case 118:

/* Line 1806 of yacc.c  */
#line 967 "SDP_parser.y"
    {
				(yyval.t_sdp_repeat) = new SDP__repeat();
				(*(yyval.t_sdp_repeat)).repeat__interval() = *(yyvsp[(2) - (7)].t_sdp_typed_time);
				delete (yyvsp[(2) - (7)].t_sdp_typed_time);
				(*(yyval.t_sdp_repeat)).active() = *(yyvsp[(4) - (7)].t_sdp_typed_time);
				delete (yyvsp[(4) - (7)].t_sdp_typed_time);
				(*(yyval.t_sdp_repeat)).offsets() = *(yyvsp[(6) - (7)].t_sdp_typed_time_list);
				delete (yyvsp[(6) - (7)].t_sdp_typed_time_list);
			}
    break;

  case 119:

/* Line 1806 of yacc.c  */
#line 978 "SDP_parser.y"
    {
				(yyval.t_sdp_typed_time_list) = new SDP__typed__time__list();
				(*(yyval.t_sdp_typed_time_list))[0] = *(yyvsp[(1) - (1)].t_sdp_typed_time);
				delete (yyvsp[(1) - (1)].t_sdp_typed_time);
			}
    break;

  case 120:

/* Line 1806 of yacc.c  */
#line 983 "SDP_parser.y"
    {
				int off_size = (*(yyvsp[(1) - (3)].t_sdp_typed_time_list)).size_of();
				(*(yyval.t_sdp_typed_time_list))[off_size] = *(yyvsp[(3) - (3)].t_sdp_typed_time);
				delete (yyvsp[(3) - (3)].t_sdp_typed_time);
			}
    break;

  case 121:

/* Line 1806 of yacc.c  */
#line 989 "SDP_parser.y"
    {(yyval.t_sdp_timezone_list) = NULL;}
    break;

  case 122:

/* Line 1806 of yacc.c  */
#line 990 "SDP_parser.y"
    {
					(yyval.t_sdp_timezone_list) = (yyvsp[(2) - (3)].t_sdp_timezone_list);
				}
    break;

  case 123:

/* Line 1806 of yacc.c  */
#line 994 "SDP_parser.y"
    {
					(yyval.t_sdp_timezone_list) = new SDP__timezone__list();
					(*(yyval.t_sdp_timezone_list))[0] = *(yyvsp[(1) - (1)].t_sdp_timezone);
					delete (yyvsp[(1) - (1)].t_sdp_timezone);
				}
    break;

  case 124:

/* Line 1806 of yacc.c  */
#line 999 "SDP_parser.y"
    {
					int tz_num = (*(yyvsp[(1) - (3)].t_sdp_timezone_list)).size_of();
					(*(yyvsp[(1) - (3)].t_sdp_timezone_list))[tz_num] = *(yyvsp[(3) - (3)].t_sdp_timezone);
					delete (yyvsp[(3) - (3)].t_sdp_timezone);
					(yyval.t_sdp_timezone_list) = (yyvsp[(1) - (3)].t_sdp_timezone_list);
				}
    break;

  case 125:

/* Line 1806 of yacc.c  */
#line 1006 "SDP_parser.y"
    {
					(yyval.t_sdp_timezone) = new SDP__timezone();
					(*(yyval.t_sdp_timezone)).adjustment__time() = int2str(*(yyvsp[(1) - (3)].intnum));
					delete (yyvsp[(1) - (3)].intnum);
					(*(yyval.t_sdp_timezone)).offset() = *(yyvsp[(3) - (3)].t_sdp_typed_time);
					delete (yyvsp[(3) - (3)].t_sdp_typed_time);
				}
    break;

  case 126:

/* Line 1806 of yacc.c  */
#line 1014 "SDP_parser.y"
    {
					(yyval.t_sdp_typed_time) = new SDP__typed__time();
					(*(yyval.t_sdp_typed_time)).time() = *(yyvsp[(1) - (1)].intnum);
					delete (yyvsp[(1) - (1)].intnum);
					(*(yyval.t_sdp_typed_time)).unit() = OMIT_VALUE;
				}
    break;

  case 127:

/* Line 1806 of yacc.c  */
#line 1020 "SDP_parser.y"
    {
					(yyval.t_sdp_typed_time) = new SDP__typed__time();
					(*(yyval.t_sdp_typed_time)).time() = *(yyvsp[(1) - (2)].intnum);
					delete (yyvsp[(1) - (2)].intnum);
					(*(yyval.t_sdp_typed_time)).unit() = CHARSTRING(1, (const char*)&(yyvsp[(2) - (2)].byte));
				}
    break;

  case 128:

/* Line 1806 of yacc.c  */
#line 1027 "SDP_parser.y"
    { // start_time SPACE stop_time
					(yyval.t_sdp_time_field) = new SDP__time__field();
					(*(yyval.t_sdp_time_field)).start__time() = (*(yyvsp[(2) - (5)].t_charstring));
					delete (yyvsp[(2) - (5)].t_charstring);
					(*(yyval.t_sdp_time_field)).stop__time() = (*(yyvsp[(4) - (5)].t_charstring));
					delete (yyvsp[(4) - (5)].t_charstring);
				}
    break;

  case 129:

/* Line 1806 of yacc.c  */
#line 1035 "SDP_parser.y"
    {(yyval.t_bandwidth_list) = NULL;}
    break;

  case 130:

/* Line 1806 of yacc.c  */
#line 1036 "SDP_parser.y"
    {
					if ((yyvsp[(1) - (2)].t_bandwidth_list) != NULL) {
						int bandwidth_num = (*(yyvsp[(1) - (2)].t_bandwidth_list)).size_of();
						(*(yyvsp[(1) - (2)].t_bandwidth_list))[bandwidth_num] = *(yyvsp[(2) - (2)].t_sdp_bandwidth);
						delete (yyvsp[(2) - (2)].t_sdp_bandwidth);
						(yyval.t_bandwidth_list) = (yyvsp[(1) - (2)].t_bandwidth_list);
					} else {
						(yyval.t_bandwidth_list) = new SDP__bandwidth__list();
						(*(yyval.t_bandwidth_list))[0] = *(yyvsp[(2) - (2)].t_sdp_bandwidth);
						delete (yyvsp[(2) - (2)].t_sdp_bandwidth);
					}
				}
    break;

  case 131:

/* Line 1806 of yacc.c  */
#line 1050 "SDP_parser.y"
    {
						(yyval.t_sdp_bandwidth) = new SDP__bandwidth();
						(*(yyval.t_sdp_bandwidth)).modifier() = *(yyvsp[(2) - (5)].t_charstring);
						delete (yyvsp[(2) - (5)].t_charstring);
						(*(yyval.t_sdp_bandwidth)).bandwidth() = str2int(*(yyvsp[(4) - (5)].t_charstring));
						delete (yyvsp[(4) - (5)].t_charstring);
					}
    break;

  case 132:

/* Line 1806 of yacc.c  */
#line 1059 "SDP_parser.y"
    {(yyval.t_sdp_connection_list) = NULL;}
    break;

  case 133:

/* Line 1806 of yacc.c  */
#line 1060 "SDP_parser.y"
    {
						if ((yyvsp[(1) - (2)].t_sdp_connection_list) != NULL) {
							int con_num = (*(yyvsp[(1) - (2)].t_sdp_connection_list)).size_of();
							(*(yyvsp[(1) - (2)].t_sdp_connection_list))[con_num] = *(yyvsp[(2) - (2)].t_sdp_connection);
							delete (yyvsp[(2) - (2)].t_sdp_connection);
							(yyval.t_sdp_connection_list) = (yyvsp[(1) - (2)].t_sdp_connection_list);
						} else {
							(yyval.t_sdp_connection_list) = new SDP__connection__list();
							(*(yyval.t_sdp_connection_list))[0] = *(yyvsp[(2) - (2)].t_sdp_connection);
							delete (yyvsp[(2) - (2)].t_sdp_connection);
						}
					}
    break;

  case 134:

/* Line 1806 of yacc.c  */
#line 1074 "SDP_parser.y"
    {(yyval.t_sdp_connection) = NULL;}
    break;

  case 135:

/* Line 1806 of yacc.c  */
#line 1075 "SDP_parser.y"
    {
						(yyval.t_sdp_connection) = new SDP__connection();
						(*(yyval.t_sdp_connection)).net__type() = *(yyvsp[(2) - (7)].t_charstring);
						delete (yyvsp[(2) - (7)].t_charstring);
						(*(yyval.t_sdp_connection)).addr__type() = *(yyvsp[(4) - (7)].t_charstring);
						delete (yyvsp[(4) - (7)].t_charstring);
						(*(yyval.t_sdp_connection)).conn__addr() = (*(yyvsp[(6) - (7)].t_sdp_conn_addr));
						delete (yyvsp[(6) - (7)].t_sdp_conn_addr);
					}
    break;

  case 136:

/* Line 1806 of yacc.c  */
#line 1086 "SDP_parser.y"
    {
						(yyval.t_sdp_connection) = new SDP__connection();
						(*(yyval.t_sdp_connection)).net__type() = *(yyvsp[(2) - (7)].t_charstring);
						delete (yyvsp[(2) - (7)].t_charstring);
						(*(yyval.t_sdp_connection)).addr__type() = *(yyvsp[(4) - (7)].t_charstring);
						delete (yyvsp[(4) - (7)].t_charstring);
						(*(yyval.t_sdp_connection)).conn__addr() = (*(yyvsp[(6) - (7)].t_sdp_conn_addr));
						delete (yyvsp[(6) - (7)].t_sdp_conn_addr);
					}
    break;

  case 137:

/* Line 1806 of yacc.c  */
#line 1095 "SDP_parser.y"
    {
					(yyval.t_sdp_conn_addr) = new SDP__conn__addr();
					(*(yyval.t_sdp_conn_addr)).addr() = (*(yyvsp[(1) - (1)].t_charstring));
					delete (yyvsp[(1) - (1)].t_charstring);
					(*(yyval.t_sdp_conn_addr)).ttl() = OMIT_VALUE;
					(*(yyval.t_sdp_conn_addr)).num__of__addr() = OMIT_VALUE;
				}
    break;

  case 138:

/* Line 1806 of yacc.c  */
#line 1102 "SDP_parser.y"
    {
					(yyval.t_sdp_conn_addr) = new SDP__conn__addr();
					(*(yyval.t_sdp_conn_addr)).addr() = (*(yyvsp[(1) - (3)].t_charstring));
					delete (yyvsp[(1) - (3)].t_charstring);
					(*(yyval.t_sdp_conn_addr)).ttl() = str2int(*(yyvsp[(3) - (3)].t_charstring));
					(*(yyval.t_sdp_conn_addr)).num__of__addr() = OMIT_VALUE;
					delete (yyvsp[(3) - (3)].t_charstring);
				}
    break;

  case 139:

/* Line 1806 of yacc.c  */
#line 1110 "SDP_parser.y"
    {
					(yyval.t_sdp_conn_addr) = new SDP__conn__addr();
					(*(yyval.t_sdp_conn_addr)).addr() = (*(yyvsp[(1) - (5)].t_charstring));
					delete (yyvsp[(1) - (5)].t_charstring);
					(*(yyval.t_sdp_conn_addr)).ttl() = str2int(*(yyvsp[(3) - (5)].t_charstring));
					(*(yyval.t_sdp_conn_addr)).num__of__addr() = str2int(*(yyvsp[(5) - (5)].t_charstring));
					delete (yyvsp[(3) - (5)].t_charstring);
					delete (yyvsp[(5) - (5)].t_charstring);
				}
    break;

  case 140:

/* Line 1806 of yacc.c  */
#line 1121 "SDP_parser.y"
    {(yyval.t_sdp_phone_list) = NULL;}
    break;

  case 141:

/* Line 1806 of yacc.c  */
#line 1122 "SDP_parser.y"
    {
					if ((yyval.t_sdp_phone_list) == NULL) {
						(yyval.t_sdp_phone_list) = new SDP__phone__list();
						(*(yyval.t_sdp_phone_list))[0] = *(yyvsp[(2) - (2)].t_sdp_contact);
						delete (yyvsp[(2) - (2)].t_sdp_contact);
					} else {
						int num_of_phonenumbers = (*(yyvsp[(1) - (2)].t_sdp_phone_list)).size_of();
						(*(yyvsp[(1) - (2)].t_sdp_phone_list))[num_of_phonenumbers]  =*(yyvsp[(2) - (2)].t_sdp_contact);
						delete (yyvsp[(2) - (2)].t_sdp_contact);
					}
				}
    break;

  case 142:

/* Line 1806 of yacc.c  */
#line 1134 "SDP_parser.y"
    {
					(yyval.t_sdp_contact) = (yyvsp[(2) - (3)].t_sdp_contact);
				}
    break;

  case 143:

/* Line 1806 of yacc.c  */
#line 1138 "SDP_parser.y"
    {
					(yyval.t_sdp_contact) = new SDP__contact();
					(*(yyval.t_sdp_contact)).addr__or__phone() = (*(yyvsp[(1) - (1)].t_charstring));
					delete (yyvsp[(1) - (1)].t_charstring);
					(*(yyval.t_sdp_contact)).disp__name() = OMIT_VALUE;
				}
    break;

  case 144:

/* Line 1806 of yacc.c  */
#line 1144 "SDP_parser.y"
    {
					(yyval.t_sdp_contact) = new SDP__contact();
					(*(yyval.t_sdp_contact)).addr__or__phone() = *(yyvsp[(1) - (4)].t_charstring);
					delete (yyvsp[(1) - (4)].t_charstring);
					(*(yyval.t_sdp_contact)).disp__name() = *(yyvsp[(3) - (4)].t_charstring);
					delete (yyvsp[(3) - (4)].t_charstring);
				}
    break;

  case 145:

/* Line 1806 of yacc.c  */
#line 1152 "SDP_parser.y"
    {
					/* phone is any charstring, but with trailing space removed*/
					(yyval.t_sdp_contact) = new SDP__contact();
					(*(yyval.t_sdp_contact)).addr__or__phone() = *(yyvsp[(3) - (4)].t_charstring);
					delete (yyvsp[(3) - (4)].t_charstring);
					(*(yyval.t_sdp_contact)).disp__name() = *(yyvsp[(1) - (4)].t_charstring);
					delete (yyvsp[(1) - (4)].t_charstring);
				}
    break;

  case 146:

/* Line 1806 of yacc.c  */
#line 1161 "SDP_parser.y"
    {
			/* remove trailing space, if any */
/* This is a rather ugly post-processing, but otherwise SDP can not
be parsed with an LR(1) parser. Reason: phone number may contain spaces, 
and delimiter between the phone number and the rest of the phone field may be a space*/

			int size = (*(yyvsp[(1) - (1)].t_charstring)).lengthof();
			
			const char* buf = (const char*)(*(yyvsp[(1) - (1)].t_charstring));
			
			if (buf[size-1] == ' ') {
				(yyval.t_charstring) = new CHARSTRING(size-1, (const char*)(*(yyvsp[(1) - (1)].t_charstring)));
				delete (yyvsp[(1) - (1)].t_charstring);
			} else
				(yyval.t_charstring) = (yyvsp[(1) - (1)].t_charstring);
		}
    break;

  case 147:

/* Line 1806 of yacc.c  */
#line 1178 "SDP_parser.y"
    {(yyval.t_sdp_email_list) = NULL;}
    break;

  case 148:

/* Line 1806 of yacc.c  */
#line 1179 "SDP_parser.y"
    {
					if ((yyvsp[(1) - (2)].t_sdp_email_list) == NULL) {
						(yyval.t_sdp_email_list) = new SDP__email__list();
						(*(yyval.t_sdp_email_list))[0] = *(yyvsp[(2) - (2)].t_sdp_contact);
						delete (yyvsp[(2) - (2)].t_sdp_contact);
					} else {
						int num_of_emails = (*(yyvsp[(1) - (2)].t_sdp_email_list)).size_of();
						// indexing begins with 0
						(*(yyvsp[(1) - (2)].t_sdp_email_list))[num_of_emails] = *(yyvsp[(2) - (2)].t_sdp_contact);
						delete (yyvsp[(2) - (2)].t_sdp_contact);
					}
				}
    break;

  case 149:

/* Line 1806 of yacc.c  */
#line 1192 "SDP_parser.y"
    {
					(yyval.t_sdp_contact) = (yyvsp[(2) - (3)].t_sdp_contact);
				}
    break;

  case 150:

/* Line 1806 of yacc.c  */
#line 1196 "SDP_parser.y"
    {
					(yyval.t_sdp_contact) = new SDP__contact();
					(*(yyval.t_sdp_contact)).addr__or__phone() = *(yyvsp[(1) - (1)].t_charstring);
					delete (yyvsp[(1) - (1)].t_charstring);
					(*(yyval.t_sdp_contact)).disp__name() = OMIT_VALUE;
				}
    break;

  case 151:

/* Line 1806 of yacc.c  */
#line 1203 "SDP_parser.y"
    {
					(yyval.t_sdp_contact) = new SDP__contact();
					(*(yyval.t_sdp_contact)).addr__or__phone() = *(yyvsp[(1) - (5)].t_charstring);
					delete (yyvsp[(1) - (5)].t_charstring);
					(*(yyval.t_sdp_contact)).disp__name() = *(yyvsp[(4) - (5)].t_charstring);
					delete (yyvsp[(4) - (5)].t_charstring);
				}
    break;

  case 152:

/* Line 1806 of yacc.c  */
#line 1210 "SDP_parser.y"
    {
					(yyval.t_sdp_contact) = new SDP__contact();
					(*(yyval.t_sdp_contact)).addr__or__phone() = *(yyvsp[(4) - (5)].t_charstring);
					delete (yyvsp[(4) - (5)].t_charstring);
					(*(yyval.t_sdp_contact)).disp__name() = *(yyvsp[(1) - (5)].t_charstring);
					delete (yyvsp[(1) - (5)].t_charstring);
				}
    break;

  case 153:

/* Line 1806 of yacc.c  */
#line 1217 "SDP_parser.y"
    {
					(yyval.t_sdp_contact) = new SDP__contact();
					(*(yyval.t_sdp_contact)).addr__or__phone() = *(yyvsp[(2) - (3)].t_charstring);
					delete (yyvsp[(2) - (3)].t_charstring);
					(*(yyval.t_sdp_contact)).disp__name() = OMIT_VALUE;
				}
    break;

  case 154:

/* Line 1806 of yacc.c  */
#line 1227 "SDP_parser.y"
    {(yyval.t_charstring) = NULL;}
    break;

  case 155:

/* Line 1806 of yacc.c  */
#line 1228 "SDP_parser.y"
    {
				(yyval.t_charstring) = (yyvsp[(2) - (3)].t_charstring);
			}
    break;

  case 156:

/* Line 1806 of yacc.c  */
#line 1233 "SDP_parser.y"
    {(yyval.t_charstring) = NULL;}
    break;

  case 157:

/* Line 1806 of yacc.c  */
#line 1234 "SDP_parser.y"
    {
						(yyval.t_charstring) = (yyvsp[(2) - (3)].t_charstring);
					}
    break;

  case 158:

/* Line 1806 of yacc.c  */
#line 1238 "SDP_parser.y"
    {
						(yyval.t_charstring) = (yyvsp[(2) - (3)].t_charstring);
				 }
    break;

  case 159:

/* Line 1806 of yacc.c  */
#line 1242 "SDP_parser.y"
    {(yyval.number) = *(yyvsp[(2) - (3)].intnum); delete (yyvsp[(2) - (3)].intnum);}
    break;

  case 160:

/* Line 1806 of yacc.c  */
#line 1245 "SDP_parser.y"
    {
					(yyval.t_sdp_origin) = new SDP__Origin();
					(*(yyval.t_sdp_origin)).user__name() = *(yyvsp[(2) - (13)].t_charstring);
					delete (yyvsp[(2) - (13)].t_charstring);
					(*(yyval.t_sdp_origin)).session__id() = *(yyvsp[(4) - (13)].t_charstring);
					delete (yyvsp[(4) - (13)].t_charstring);
					(*(yyval.t_sdp_origin)).session__version() = *(yyvsp[(6) - (13)].t_charstring);
					delete (yyvsp[(6) - (13)].t_charstring);
					(*(yyval.t_sdp_origin)).net__type() = *(yyvsp[(8) - (13)].t_charstring);
					delete (yyvsp[(8) - (13)].t_charstring);
					(*(yyval.t_sdp_origin)).addr__type() = *(yyvsp[(10) - (13)].t_charstring);
					delete (yyvsp[(10) - (13)].t_charstring);
					(*(yyval.t_sdp_origin)).addr() = *(yyvsp[(12) - (13)].t_charstring);
					delete (yyvsp[(12) - (13)].t_charstring);
				}
    break;

  case 161:

/* Line 1806 of yacc.c  */
#line 1266 "SDP_parser.y"
    {(yyval.t_charstring) = new CHARSTRING(1, (const char*)&(yyvsp[(1) - (1)].byte));}
    break;

  case 162:

/* Line 1806 of yacc.c  */
#line 1267 "SDP_parser.y"
    {
				const CHARSTRING& str = *(yyvsp[(1) - (2)].t_charstring) + CHARSTRING(1, (const char*)&(yyvsp[(2) - (2)].byte));
				delete (yyvsp[(1) - (2)].t_charstring);
				(yyval.t_charstring) = new CHARSTRING(str);
			}
    break;

  case 163:

/* Line 1806 of yacc.c  */
#line 1276 "SDP_parser.y"
    {(yyval.byte) = (yyvsp[(1) - (1)].byte);}
    break;

  case 164:

/* Line 1806 of yacc.c  */
#line 1277 "SDP_parser.y"
    {(yyval.byte) = (yyvsp[(1) - (1)].number) + 0x30;}
    break;

  case 165:

/* Line 1806 of yacc.c  */
#line 1280 "SDP_parser.y"
    {(yyval.number) = 0;}
    break;

  case 166:

/* Line 1806 of yacc.c  */
#line 1281 "SDP_parser.y"
    {(yyval.number) = (yyvsp[(1) - (1)].number);}
    break;

  case 167:

/* Line 1806 of yacc.c  */
#line 1283 "SDP_parser.y"
    {(yyval.number) = 1;}
    break;

  case 168:

/* Line 1806 of yacc.c  */
#line 1284 "SDP_parser.y"
    {(yyval.number) = 2;}
    break;

  case 169:

/* Line 1806 of yacc.c  */
#line 1285 "SDP_parser.y"
    {(yyval.number) = 3;}
    break;

  case 170:

/* Line 1806 of yacc.c  */
#line 1286 "SDP_parser.y"
    {(yyval.number) = 4;}
    break;

  case 171:

/* Line 1806 of yacc.c  */
#line 1287 "SDP_parser.y"
    {(yyval.number) = 5;}
    break;

  case 172:

/* Line 1806 of yacc.c  */
#line 1288 "SDP_parser.y"
    {(yyval.number) = 6;}
    break;

  case 173:

/* Line 1806 of yacc.c  */
#line 1289 "SDP_parser.y"
    {(yyval.number) = 7;}
    break;

  case 174:

/* Line 1806 of yacc.c  */
#line 1290 "SDP_parser.y"
    {(yyval.number) = 8;}
    break;

  case 175:

/* Line 1806 of yacc.c  */
#line 1291 "SDP_parser.y"
    {(yyval.number) = 9;}
    break;

  case 176:

/* Line 1806 of yacc.c  */
#line 1295 "SDP_parser.y"
    {(yyval.byte) = (yyvsp[(1) - (1)].byte);}
    break;

  case 177:

/* Line 1806 of yacc.c  */
#line 1296 "SDP_parser.y"
    {(yyval.byte) = (yyvsp[(1) - (1)].byte);}
    break;

  case 178:

/* Line 1806 of yacc.c  */
#line 1297 "SDP_parser.y"
    {(yyval.byte) = (yyvsp[(1) - (1)].byte);}
    break;

  case 179:

/* Line 1806 of yacc.c  */
#line 1298 "SDP_parser.y"
    {(yyval.byte) = (yyvsp[(1) - (1)].byte);}
    break;

  case 180:

/* Line 1806 of yacc.c  */
#line 1299 "SDP_parser.y"
    {(yyval.byte) = (yyvsp[(1) - (1)].byte);}
    break;

  case 181:

/* Line 1806 of yacc.c  */
#line 1300 "SDP_parser.y"
    {(yyval.byte) = (yyvsp[(1) - (1)].byte);}
    break;

  case 182:

/* Line 1806 of yacc.c  */
#line 1301 "SDP_parser.y"
    {(yyval.byte) = (yyvsp[(1) - (1)].byte);}
    break;

  case 183:

/* Line 1806 of yacc.c  */
#line 1302 "SDP_parser.y"
    {(yyval.byte) = (yyvsp[(1) - (1)].byte);}
    break;

  case 184:

/* Line 1806 of yacc.c  */
#line 1303 "SDP_parser.y"
    {(yyval.byte) = (yyvsp[(1) - (1)].byte);}
    break;



/* Line 1806 of yacc.c  */
#line 4063 "SDP_parse_.tab.c"
      default: break;
    }
  /* User semantic actions sometimes alter yychar, and that requires
     that yytoken be updated with the new translation.  We take the
     approach of translating immediately before every use of yytoken.
     One alternative is translating here after every semantic action,
     but that translation would be missed if the semantic action invokes
     YYABORT, YYACCEPT, or YYERROR immediately after altering yychar or
     if it invokes YYBACKUP.  In the case of YYABORT or YYACCEPT, an
     incorrect destructor might then be invoked immediately.  In the
     case of YYERROR or YYBACKUP, subsequent parser actions might lead
     to an incorrect destructor call or verbose syntax error message
     before the lookahead is translated.  */
  YY_SYMBOL_PRINT ("-> $$ =", yyr1[yyn], &yyval, &yyloc);

  YYPOPSTACK (yylen);
  yylen = 0;
  YY_STACK_PRINT (yyss, yyssp);

  *++yyvsp = yyval;

  /* Now `shift' the result of the reduction.  Determine what state
     that goes to, based on the state we popped back to and the rule
     number reduced by.  */

  yyn = yyr1[yyn];

  yystate = yypgoto[yyn - YYNTOKENS] + *yyssp;
  if (0 <= yystate && yystate <= YYLAST && yycheck[yystate] == *yyssp)
    yystate = yytable[yystate];
  else
    yystate = yydefgoto[yyn - YYNTOKENS];

  goto yynewstate;


/*------------------------------------.
| yyerrlab -- here on detecting error |
`------------------------------------*/
yyerrlab:
  /* Make sure we have latest lookahead translation.  See comments at
     user semantic actions for why this is necessary.  */
  yytoken = yychar == YYEMPTY ? YYEMPTY : YYTRANSLATE (yychar);

  /* If not already recovering from an error, report this error.  */
  if (!yyerrstatus)
    {
      ++yynerrs;
#if ! YYERROR_VERBOSE
      yyerror (YY_("syntax error"));
#else
# define YYSYNTAX_ERROR yysyntax_error (&yymsg_alloc, &yymsg, \
                                        yyssp, yytoken)
      {
        char const *yymsgp = YY_("syntax error");
        int yysyntax_error_status;
        yysyntax_error_status = YYSYNTAX_ERROR;
        if (yysyntax_error_status == 0)
          yymsgp = yymsg;
        else if (yysyntax_error_status == 1)
          {
            if (yymsg != yymsgbuf)
              YYSTACK_FREE (yymsg);
            yymsg = (char *) YYSTACK_ALLOC (yymsg_alloc);
            if (!yymsg)
              {
                yymsg = yymsgbuf;
                yymsg_alloc = sizeof yymsgbuf;
                yysyntax_error_status = 2;
              }
            else
              {
                yysyntax_error_status = YYSYNTAX_ERROR;
                yymsgp = yymsg;
              }
          }
        yyerror (yymsgp);
        if (yysyntax_error_status == 2)
          goto yyexhaustedlab;
      }
# undef YYSYNTAX_ERROR
#endif
    }



  if (yyerrstatus == 3)
    {
      /* If just tried and failed to reuse lookahead token after an
	 error, discard it.  */

      if (yychar <= YYEOF)
	{
	  /* Return failure if at end of input.  */
	  if (yychar == YYEOF)
	    YYABORT;
	}
      else
	{
	  yydestruct ("Error: discarding",
		      yytoken, &yylval);
	  yychar = YYEMPTY;
	}
    }

  /* Else will try to reuse lookahead token after shifting the error
     token.  */
  goto yyerrlab1;


/*---------------------------------------------------.
| yyerrorlab -- error raised explicitly by YYERROR.  |
`---------------------------------------------------*/
yyerrorlab:

  /* Pacify compilers like GCC when the user code never invokes
     YYERROR and the label yyerrorlab therefore never appears in user
     code.  */
  if (/*CONSTCOND*/ 0)
     goto yyerrorlab;

  /* Do not reclaim the symbols of the rule which action triggered
     this YYERROR.  */
  YYPOPSTACK (yylen);
  yylen = 0;
  YY_STACK_PRINT (yyss, yyssp);
  yystate = *yyssp;
  goto yyerrlab1;


/*-------------------------------------------------------------.
| yyerrlab1 -- common code for both syntax error and YYERROR.  |
`-------------------------------------------------------------*/
yyerrlab1:
  yyerrstatus = 3;	/* Each real token shifted decrements this.  */

  for (;;)
    {
      yyn = yypact[yystate];
      if (!yypact_value_is_default (yyn))
	{
	  yyn += YYTERROR;
	  if (0 <= yyn && yyn <= YYLAST && yycheck[yyn] == YYTERROR)
	    {
	      yyn = yytable[yyn];
	      if (0 < yyn)
		break;
	    }
	}

      /* Pop the current state because it cannot handle the error token.  */
      if (yyssp == yyss)
	YYABORT;


      yydestruct ("Error: popping",
		  yystos[yystate], yyvsp);
      YYPOPSTACK (1);
      yystate = *yyssp;
      YY_STACK_PRINT (yyss, yyssp);
    }

  *++yyvsp = yylval;


  /* Shift the error token.  */
  YY_SYMBOL_PRINT ("Shifting", yystos[yyn], yyvsp, yylsp);

  yystate = yyn;
  goto yynewstate;


/*-------------------------------------.
| yyacceptlab -- YYACCEPT comes here.  |
`-------------------------------------*/
yyacceptlab:
  yyresult = 0;
  goto yyreturn;

/*-----------------------------------.
| yyabortlab -- YYABORT comes here.  |
`-----------------------------------*/
yyabortlab:
  yyresult = 1;
  goto yyreturn;

#if !defined(yyoverflow) || YYERROR_VERBOSE
/*-------------------------------------------------.
| yyexhaustedlab -- memory exhaustion comes here.  |
`-------------------------------------------------*/
yyexhaustedlab:
  yyerror (YY_("memory exhausted"));
  yyresult = 2;
  /* Fall through.  */
#endif

yyreturn:
  if (yychar != YYEMPTY)
    {
      /* Make sure we have latest lookahead translation.  See comments at
         user semantic actions for why this is necessary.  */
      yytoken = YYTRANSLATE (yychar);
      yydestruct ("Cleanup: discarding lookahead",
                  yytoken, &yylval);
    }
  /* Do not reclaim the symbols of the rule which action triggered
     this YYABORT or YYACCEPT.  */
  YYPOPSTACK (yylen);
  YY_STACK_PRINT (yyss, yyssp);
  while (yyssp != yyss)
    {
      yydestruct ("Cleanup: popping",
		  yystos[*yyssp], yyvsp);
      YYPOPSTACK (1);
    }
#ifndef yyoverflow
  if (yyss != yyssa)
    YYSTACK_FREE (yyss);
#endif
#if YYERROR_VERBOSE
  if (yymsg != yymsgbuf)
    YYSTACK_FREE (yymsg);
#endif
  /* Make sure YYID is used.  */
  return YYID (yyresult);
}



/* Line 2067 of yacc.c  */
#line 1305 "SDP_parser.y"


/*Additional C code*/

