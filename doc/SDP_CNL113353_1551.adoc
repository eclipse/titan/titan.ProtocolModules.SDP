---
Author: Gábor Szalai
Version: 1551- CNL 113 353, Rev. C
Date: 2016-08-10

---
= SDP Protocol Module for TTCN-3 Toolset with TITAN, Description
:author: Gábor Szalai
:revnumber: 1551- CNL 113 353, Rev. C
:revdate: 2016-08-10
:toc:

== How to Read This Document

This is the User Guide for the SDP protocol module. The SDP protocol module is developed for the TTCN-3 Toolset with TITAN.

== Presumed Knowledge

To use this protocol module the knowledge of the TTCN-3 language <<_2, [2]>> is essential.

== System Requirements

Protocol modules are a set of TTCN-3 source code files that can be used as part of TTCN-3 test suites only. Hence, protocol modules alone do not put specific requirements on the system used. However in order to compile and execute a TTCN-3 test suite using the set of protocol modules the following system requirements must be satisfied:

* TITAN TTCN-3 Test Executor R7A (1.7.pl0) or higher installed. For installation guide see <<_3, [3]>>.

NOTE: This version of the protocol module is not compatible with TITAN releases earlier than R7A.

= Functionality

== Protocol Version Implemented

This set of protocol modules implements protocol messages and constants of the SDP protocol, (see <<_1, [1]>>) with the modifications specified in ‎3.2. Besides the SDP attributes listed in [1] the following SDP attributes are implemented:

[width="100%",cols="50%,50%",options="header",]
|====================================================================================================================================================
|RFC |Attributes
|draft-ietf-simple-message-sessions-12 +
The Message Session Relay Protocol <<_5, ‎[5]>> |accept-types, +
accept-wrapped-types, +
max-size, +
path
|RFC 3890 +
A Transport Independent Bandwidth Modifier for the Session Description Protocol (SDP) ‎<<_6, [6]>> |
pathmaxprate
|RFC 3388 +
Grouping of Media Lines in the Session Description Protocol (SDP) ‎‎<<_7, [7]>>|mid, +
group
|RFC 4145 +
TCP-Based Media Transport in the Session Description Protocol (SDP) ‎<<_8, [8]>> |setup, +
connection
|RFC 4568 +
Session Description Protocol (SDP) Security Descriptions for Media Streams |crypto
|RFC 5245 +
Interactive Connectivity Establishment (ICE) ‎<<_10, [10]>> |ice-ufrag, ice-pwd, candidate, remote-candidates, ice-lite, ice-mismatch, and ice-options
|RFC 4585 +
Extended RTP Profile for Real-time Transport Control Protocol (RTCP)-Based Feedback (RTP/AVPF) ‎<<_11, [11]>> |rtcp-fb
|RFC 3362 +
Real-time Facsimile (T.38) – image/t.38 MIME Sub-type Registration |T38Fax
|RFC 5547 |file-disposition file-date file-icon file-range
|draft-ietf-mmusic-sctp-sdp-08 & draft-ietf-mmusic-sctp-sdp-11 |sctpmap, sctp_port, max-message-size
|====================================================================================================================================================

== Routing Functionality

Routing functionality is not performed.

== Modified and Non-Implemented Protocol Elements

=== Relaxed Conditions

There is no constraint between received and sent messages.

== Ericsson-Specific Changes

There is no Ericsson specific change in this product.

== Backward Incompatibilities

None.

[[protocol-modifications-deviations]]
== Protocol Modifications/Deviations

Protocol modules contain the following modifications/deviations from <<_1, [1]>> changing the protocol message structure/behavior:

* In the TITAN executor environment, integer values are stored as 32-bit signed integers. Hence, those variables that represent an integer value greater than `_231-1_` cannot be stored in a variable of type integer. This affects the following information elements:
+
--
** Time field: start time, stop time

** Zone adjustment time

** Origin field: session ID, session version
--
+
These integer values are represented as charstrings.

* At most one bandwidth field is allowed at session level and at most one for each media description. (The standard <<_1, [1]>> specifies it ambiguously: the grammar allows multiple bandwidth fields, but the textual description does not allow this.)

* The decoder supports all three variants of email-address and phone-number formats (see <<_1, [1]>> Appendix A). By default, the encoder encodes these fields in the following form: email (displayable name) and phone (displayable name). If you want to encode these fields in the form: displayable name `<email>` and displayable name `<phone>`, use the __f_SDP_enc_Message_RFC822Name__ encoder method for encoding (see <<_4, [4]>>).

The language specified by <<_1, [1]>> can not be used for creating an LR(1) parser. Hence, this implementation contains a simplified parser, which does not constrain the format of text fields. This should not cause any trouble when decoding SDP messages. However, content of fields should be verified in TTCN, when needed.

The encoder does not perform any format check on charstring fields. It is the user’s responsibility to ensure that charstring fields contain valid data.

= Feature List

[[encoding-decoding-and-other-related-functions]]
== Encoding/Decoding and Other Related Functions

This product contains encoding/decoding functions, which assure correct encoding of messages when sent from Titan and correct decoding of messages when received by Titan. Implemented encoding/decoding functions:

[cols=3*,options=header]
|===

|Name
|Type of formal parameters
|Type of return value

|`f_SDP_enc_Message`
|(in SDP_Message)
|return charstring

|`f_SDP_enc_Message_RFC822Name`
|(in SDP_Message)
|return charstring

|`f_SDP_dec_Message`
|(in charstring)
|return SDP_Message

|`f_SDP_decodeMessage`
|(in charstring input, out SDP_Message msg)
|return integer

|`f_SDP_enc_SRTP_Crypto`
|(in SRTP_Crypto input)
|return charstring;

|`f_SDP_dec_SRTP_Crypto`
|(in charstring input)
|return SRTP_Crypto
|===

The function `f_SDP_dec_Message` decodes charstrings that represent an encoded SDP message. The function `f_SDP_enc_Message` encodes an SDP message given as TTCN-3 data into a character string. The function `f_SDP_enc_Message_RFC822Name` performs the same task as the `f_SDP_enc_Message`, but the email/phone address will be in form that RFC 822 specifies: displayable name `<email>` and displayable name `<phone>` (see <<_5, [5]>>).

= Protocol Modules

== Overview

Protocol modules implement the message structures of the related protocol in a formalized way, using the standard specification language TTCN-3 . This allows defining of test data (templates) in the TTCN-3 language <<_6, [6]>> and correctly encoding/decoding messages when executing test suites using the Titan TTCN-3 test environment <<_4, [4]>>.

Protocol modules are using Titan’s RAW encoding attributes <<_4, [4]>> and hence is usable with the Titan test toolset only.

== Installation

The set of protocol modules can be used in developing TTCN-3 test suites using any text editor. However to make the work more efficient a TTCN-3-enabled text editor is recommended (e.g.`nedit`, `xemacs`). Since the SDP protocol is used as a part of a TTCN-3 test suite, this requires TTCN-3 Test Executor be installed before the module can be compiled and executed together with other parts of the test suite. For more details on the installation of TTCN-3 Test Executor see the relevant section of <<_4, [4]>>.

Release of the protocol module contains a demo directory. The demo directory contains 3 files:

* _SDP.prj_ - Titan GUI project file

* __SDP_Templates.ttcn__ - Test SDP messages

* __SDP_test.ttcn__ - Encoding/decoding test case to encode/decode messages listed in __SDP_Templates.ttcn__.

== Configuration

None.

== Parser Generation Rules

In order to generate the _.c_ and _.h_ files from _.y_ and _.l_ the following _Makefile_ rules must be used:

[source]
----
SDP_parse_.tab.c SDP_parse_.tab.h: SDP_parser.y

bison -dv -p SDP_parse_ -b SDP_parse_ $<

lex.SDP_parse_.c: SDP_parser.l

flex -Cfr -8 -Bvpp -PSDP_parse_ SDP_parser.l
----

The _.h_ and _.c_ parser files should be generated during the protocol module development. Only the pregenerated files are needed for test case development and test execution.

= Terminology

No specific terminology used.

== Abbreviations

IP:: Internet Protocol

SDP:: Session Description Protocol

TTCN-3:: Testing and Test Control Notation version 3

= References

[[_1]]
[1] https://tools.ietf.org/html/rfc2327[RFC 2327] +
SDP: Session Description Protocol

[[_2]]
[2] ETSI ES 201 873-1 v.2.2.1 (02/2003) +
The Testing and Test Control Notation version 3. Part 1: Core Language

[[_3]]
[3] User Documentation for the TITAN TTCN-3 Test Executor

[[_4]]
[4] https://tools.ietf.org/html/rfc822[RFC 822] +
Standard for the format of ARPA Internet text messages

[[_5]]
[5] https://tools.ietf.org/html/draft-ietf-simple-message-sessions-12 +
The Message Session Relay Protocol

[[_6]]
[6] https://tools.ietf.org/html/rfc3890[RFC 3890] +
A Transport Independent Bandwidth Modifier for the Session Description Protocol (SDP)

[[_7]]
[7] https://tools.ietf.org/html/rfc3388[RFC3388] +
Grouping of Media Lines in the Session Description Protocol (SDP)

[[_8]]
[8] https://tools.ietf.org/html/rfc4145[RFC 4145] +
TCP-Based Media Transport in the Session Description Protocol (SDP)

[[_9]]
[9] https://tools.ietf.org/html/rfc4568[RFC 4568] +
Session Description Protocol (SDP) Security Descriptions for Media Streams

[[_10]]
[10] https://tools.ietf.org/html/rfc5245[RFC 5245] +
Interactive Connectivity Establishment (ICE)

[[_11]]
[11] https://tools.ietf.org/html/rfc4585[RFC 4585] +
Extended RTP Profile for Real-time Transport Control Protocol (RTCP)-Based Feedback (RTP/AVPF)

[[_12]]
[12] https://tools.ietf.org/html/rfc3362[RFC 3362] +
Real-time Facsimile (T.38) – image/t.38 MIME Sub-type Registration
